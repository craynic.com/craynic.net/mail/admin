// noinspection JSUnresolvedFunction

const adminDb = new Mongo().getDB("admin");

adminDb.createUser({
    user: 'user',
    pwd: 'password',
    roles: [
        {role: 'readWrite', db: 'db'},
        {role: 'dbAdmin', db: 'db'},
        {role: 'readWrite', db: 'db_test'},
        {role: 'dbAdmin', db: 'db_test'}
    ]
});
