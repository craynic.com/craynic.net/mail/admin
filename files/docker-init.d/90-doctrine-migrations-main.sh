#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$APP_DB_DSN" ]]; then
  echo "No APP_DB_DSN set, cannot run migrations."
  exit 1
fi

WAIT_FOR_DB_TIMEOUT=30
MYSQL_DEFAULT_PORT="3306"

MYSQL_HOST=$(php -r "\$parsed = parse_url('${APP_DB_DSN}');
echo sprintf('%s:%d', \$parsed['host'], \$parsed['port'] ?? $MYSQL_DEFAULT_PORT);"
);

./vendor/bin/wait-for-it.sh "$MYSQL_HOST" -t "$WAIT_FOR_DB_TIMEOUT"

# run the migrations
./bin/console doctrine:migrations:migrate \
  --no-interaction \
  --configuration=config/packages/migrations/main.yaml
