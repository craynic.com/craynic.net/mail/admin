#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$APP_MONGO_CONN_URI" || -z "$APP_MONGO_DB" ]]; then
  echo "MongoDB credentials not set, cannot run migrations"
  exit 1
fi

export APP_MONGO_APPNAME=mongoDbMigrations

WAIT_FOR_DB_TIMEOUT=30

./bin/console app:get-hosts-from-mongodb-connection-string "$APP_MONGO_CONN_URI" | while read -r MONGODB_HOST; do
  ./vendor/bin/wait-for-it.sh "$MONGODB_HOST" -t "$WAIT_FOR_DB_TIMEOUT"
done

# run the migrations
./bin/console mongodb:migrations:migrate --no-interaction
