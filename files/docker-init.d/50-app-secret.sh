#!/usr/bin/env bash

set -Eeuo pipefail

APP_SECRET="${APP_SECRET:-""}"

if [[ -z "$APP_SECRET" ]]; then
  echo "Please define APP_SECRET" >/dev/stderr
  exit 1
fi

exit 0
