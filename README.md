# Administration Backend for managing e-mails **

** And maybe some more, or less, in the future

## Provides

* OAuth2 authentication for Dovecot

## Installation

Provide the following ENV variables for Docker container:

* `APP_DB_DSN` - DSN connection string for MySQL, holding OAuth2 clients configuration
* `APP_DB_DSN_READMODELS` - DSN connection string for MySQL, holding readmodels
* `APP_ENV` - set `prod` for production, `dev` for development
* `APP_MONGO_CONN_URI` - DSN connection string for MongoDB, holding e-mail domains configuration
* `APP_MONGO_DB` - The name of the database holding the data
* `APP_OAUTH_ENCRYPTION_DEFUSE_KEY`

From inside the Docker container run

```shell
# creates the MySQL database tables
./bin/console doctrine:schema:create

./bin/console league:oauth2-server:create-client \
  --grant-type password --scope email dovecot
```

This gives you client secret and client identifier.
