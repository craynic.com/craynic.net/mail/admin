-- This is for the development mode - since we want to put also records in the tables,
-- we need to create the tables already here.
-- In production this is done by the MySQL migration.

CREATE TABLE `oauth2_client` (
    `identifier` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
    `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
    `secret` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
    `redirectUris` text COLLATE utf8_unicode_ci COMMENT '(DC2Type:oauth2_redirect_uri)',
    `grants` text COLLATE utf8_unicode_ci COMMENT '(DC2Type:oauth2_grant)',
    `scopes` text COLLATE utf8_unicode_ci COMMENT '(DC2Type:oauth2_scope)',
    `active` tinyint(1) NOT NULL,
    `allowPlainTextPkce` tinyint(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

CREATE TABLE `oauth2_access_token` (
    `identifier` char(80) COLLATE utf8_unicode_ci NOT NULL,
    `client` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
    `expiry` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
    `userIdentifier` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
    `scopes` text COLLATE utf8_unicode_ci COMMENT '(DC2Type:oauth2_scope)',
    `revoked` tinyint(1) NOT NULL,
    PRIMARY KEY (`identifier`),
    KEY `IDX_454D9673C7440455` (`client`),
    CONSTRAINT `FK_454D9673C7440455` FOREIGN KEY (`client`) REFERENCES `oauth2_client` (`identifier`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

CREATE TABLE `oauth2_authorization_code` (
    `identifier` char(80) COLLATE utf8_unicode_ci NOT NULL,
    `client` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
    `expiry` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
    `userIdentifier` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
    `scopes` text COLLATE utf8_unicode_ci COMMENT '(DC2Type:oauth2_scope)',
    `revoked` tinyint(1) NOT NULL,
    PRIMARY KEY (`identifier`),
    KEY `IDX_509FEF5FC7440455` (`client`),
    CONSTRAINT `FK_509FEF5FC7440455` FOREIGN KEY (`client`) REFERENCES `oauth2_client` (`identifier`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

CREATE TABLE `oauth2_refresh_token` (
    `identifier` char(80) COLLATE utf8_unicode_ci NOT NULL,
    `access_token` char(80) COLLATE utf8_unicode_ci DEFAULT NULL,
    `expiry` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
    `revoked` tinyint(1) NOT NULL,
    PRIMARY KEY (`identifier`),
    KEY `IDX_4DD90732B6A2DD68` (`access_token`),
    CONSTRAINT `FK_4DD90732B6A2DD68` FOREIGN KEY (`access_token`) REFERENCES `oauth2_access_token` (`identifier`)
        ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
