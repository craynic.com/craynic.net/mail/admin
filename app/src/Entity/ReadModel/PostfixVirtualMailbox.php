<?php

declare(strict_types=1);

namespace App\Entity\ReadModel;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity]
#[Table(name: 'postfix_virtual_mailbox_maps')]
class PostfixVirtualMailbox
{
    #[Column(type: "string")]
    #[Id]
    public string $mailbox;

    public function __construct(string $mailbox)
    {
        $this->mailbox = $mailbox;
    }
}
