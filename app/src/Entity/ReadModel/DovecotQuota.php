<?php

declare(strict_types=1);

namespace App\Entity\ReadModel;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity]
#[Table(name: 'dovecot_quota')]
class DovecotQuota
{
    #[Column(type: "string")]
    #[Id]
    public readonly string $username;

    #[Column(type: "bigint", options: ['default' => 0])]
    public int $bytes = 0;

    #[Column(type: "integer", options: ['default' => 0])]
    public int $messages = 0;
}
