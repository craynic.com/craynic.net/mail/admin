<?php

declare(strict_types=1);

namespace App\Entity\ReadModel;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity]
#[Table(name: 'dovecot_userdb')]
class DovecotUserDb
{
    #[Column(type: "string")]
    #[Id]
    public readonly string $user;

    #[Column(name: "quota_rule", type: "text", length: 65536)]
    public string $quotaRule;

    public function __construct(string $user)
    {
        $this->user = $user;
    }
}
