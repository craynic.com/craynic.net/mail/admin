<?php

declare(strict_types=1);

namespace App\Entity\ReadModel;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity]
#[Table(name: 'opendkim_keytable')]
class OpenDKIMKeyTable
{
    #[Column(type: "string")]
    #[Id]
    public readonly string $domain;

    #[Column(name: "dkim_selector", type: "string")]
    public string $dkimSelector;

    #[Column(name: "dkim_key", type: "string", length: 65536)]
    public string $dkimKey;

    public function __construct(string $domain)
    {
        $this->domain = $domain;
    }
}
