<?php

declare(strict_types=1);

namespace App\Entity\ReadModel;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity]
#[Table(name: 'postfix_virtual_alias_maps')]
class PostfixVirtualAlias
{
    #[Column(type: "string")]
    #[Id]
    public string $address;

    #[Column(type: "string")]
    #[Id]
    public string $alias;

    public function __construct(string $address, string $alias)
    {
        $this->address = $address;
        $this->alias = $alias;
    }
}
