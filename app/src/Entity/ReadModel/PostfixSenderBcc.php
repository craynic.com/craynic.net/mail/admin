<?php

declare(strict_types=1);

namespace App\Entity\ReadModel;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

#[Entity]
#[Table(name: 'postfix_sender_bcc_maps')]
class PostfixSenderBcc
{
    #[Column(type: "string")]
    #[Id]
    public readonly string $sender;

    #[Column(type: "string")]
    public string $bcc;

    public function __construct(string $sender)
    {
        $this->sender = $sender;
    }
}
