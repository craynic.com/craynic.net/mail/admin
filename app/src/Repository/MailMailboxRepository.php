<?php

declare(strict_types=1);

namespace App\Repository;

use App\Document\MailMailbox;
use App\Exception\InvalidEmailAddress;
use App\Exception\MailboxNotFound;
use App\Exception\MailDomainNotFound;
use App\Security\MailboxUser;
use App\ValueObject\EmailAddress;

final readonly class MailMailboxRepository
{
    public function __construct(private MailDomainRepository $mailDomainRepository)
    {
    }

    public function findByMailboxUser(MailboxUser $mailboxUser): MailMailbox
    {
        try {
            $emailAddress = new EmailAddress($mailboxUser->getUserIdentifier());

            return $this->mailDomainRepository
                ->findOneByDomainName($emailAddress->domain)
                ->getMailboxByName($emailAddress->mailbox);
        } catch (InvalidEmailAddress | MailDomainNotFound) {
            throw new MailboxNotFound($mailboxUser->getUserIdentifier());
        }
    }
}
