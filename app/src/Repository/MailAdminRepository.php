<?php

declare(strict_types=1);

namespace App\Repository;

use App\Document\MailAdmin;
use App\Exception\MailAdminNotFound;
use App\Security\MailboxUser;
use App\ValueObject\EmailAddress;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\Bundle\MongoDBBundle\Repository\ServiceDocumentRepository;
use Doctrine\ODM\MongoDB\Repository\DocumentRepository;
use MongoDB\BSON\ObjectIdInterface;

/**
 * @extends DocumentRepository<MailAdmin>
 */
final class MailAdminRepository extends ServiceDocumentRepository
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, MailAdmin::class);
    }

    /**
     * @return MailAdmin[]
     */
    public function getAdminsForDomainIds(ObjectIdInterface ... $domainIds): array
    {
        return $this->findBy(['$or' => [
            ['isSuperAdmin' => true],
            ['domains' => ['$in' => $domainIds]]
        ]]);
    }

    public function getAdminByEmail(EmailAddress $emailAddress): MailAdmin
    {
        return !is_null($admin = $this->findOneBy(['email' => $emailAddress]))
            ? $admin
            : throw new MailAdminNotFound((string) $emailAddress);
    }

    public function findByMailboxUser(MailboxUser $mailboxUser): MailAdmin
    {
        return $this->getAdminByEmail(
            new EmailAddress($mailboxUser->getUserIdentifier())
        );
    }
}
