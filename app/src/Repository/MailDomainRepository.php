<?php

declare(strict_types=1);

namespace App\Repository;

use App\Document\MailDomain;
use App\Exception\MailDomainNotFound;
use App\ValueObject\EmailAddress;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\Bundle\MongoDBBundle\Repository\ServiceDocumentRepository;
use Doctrine\ODM\MongoDB\Repository\DocumentRepository;
use MongoDB\BSON\Binary;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @extends DocumentRepository<MailDomain>
 */
final class MailDomainRepository extends ServiceDocumentRepository
{
    public function __construct(ManagerRegistry $managerRegistry, private readonly LoggerInterface $logger)
    {
        parent::__construct($managerRegistry, MailDomain::class);
    }

    public function findOneByDomainIdentifier(string $domainIdentifier): MailDomain
    {
        $this->logger->debug(sprintf('Getting domain by identifier "%s"...', $domainIdentifier));

        return Uuid::isValid($domainIdentifier)
            ? $this->findOneByUUID(Uuid::fromString($domainIdentifier))
            : $this->findOneByDomainName(strtolower($domainIdentifier), allowAliases: true);
    }

    public function findOneByUUID(UuidInterface $uuid): MailDomain
    {
        $this->logger->debug(sprintf('Getting domain by UUID "%s"...', $uuid->toString()));

        $binaryUuid = new Binary($uuid->getBytes(), Binary::TYPE_UUID);

        $queryBuilder = $this->createQueryBuilder();
        $queryBuilder->expr()->field('id')->equals($binaryUuid);
        /** @var ?MailDomain $mailDomain */
        $mailDomain = $queryBuilder->getQuery()->getSingleResult();

        return $mailDomain
            ?? throw new MailDomainNotFound($uuid->toString());
    }

    public function findOneByRedirectIdentifier(string $redirectIdentifier): MailDomain
    {
        $this->logger->debug(sprintf('Getting domain by redirect identifier "%s"...', $redirectIdentifier));

        if (Uuid::isValid($redirectIdentifier)) {
            $redirectUUID = Uuid::fromString($redirectIdentifier);

            return $this->findOneByRedirectUUID($redirectUUID);
        }

        $emailAddress = new EmailAddress($redirectIdentifier);

        return $this->findOneByDomainName($emailAddress->domain, allowAliases: true);
    }

    public function findOneByMailboxIdentifier(string $mailboxIdentifier): MailDomain
    {
        $this->logger->debug(sprintf('Getting domain by mailbox identifier "%s"...', $mailboxIdentifier));

        if (Uuid::isValid($mailboxIdentifier)) {
            $mailboxUUID = Uuid::fromString($mailboxIdentifier);

            return $this->findOneByMailboxUUID($mailboxUUID);
        }

        $emailAddress = new EmailAddress($mailboxIdentifier);

        return $this->findOneByDomainName($emailAddress->domain, allowAliases: true);
    }

    public function findOneByDomainAlias(string $domainAlias): MailDomain
    {
        $this->logger->debug(sprintf('Getting domain by domain alias "%s"...', $domainAlias));

        return $this->findOneBy(['aliases.domain' => $domainAlias])
            ?? throw new MailDomainNotFound($domainAlias);
    }

    public function findOneByMailboxUUID(UuidInterface $uuid): MailDomain
    {
        $this->logger->debug(sprintf('Getting domain by mailbox UUID "%s"...', $uuid->toString()));

        $binaryUuid = new Binary($uuid->getBytes(), Binary::TYPE_UUID);

        $queryBuilder = $this->createQueryBuilder();
        $queryBuilder->expr()->field('mailboxes.id')->equals($binaryUuid);
        /** @var ?MailDomain $mailDomain */
        $mailDomain = $queryBuilder->getQuery()->getSingleResult();

        return $mailDomain
            ?? throw new MailDomainNotFound($uuid->toString());
    }

    public function findOneByRedirectUUID(UuidInterface $uuid): MailDomain
    {
        $this->logger->debug(sprintf('Getting domain by redirect UUID "%s"...', $uuid->toString()));

        $binaryUuid = new Binary($uuid->getBytes(), Binary::TYPE_UUID);

        $queryBuilder = $this->createQueryBuilder();
        $queryBuilder->expr()->field('redirects.id')->equals($binaryUuid);
        /** @var ?MailDomain $mailDomain */
        $mailDomain = $queryBuilder->getQuery()->getSingleResult();

        return $mailDomain
            ?? throw new MailDomainNotFound($uuid->toString());
    }

    public function findOneByDomainName(string $domainName, bool $allowAliases = false): MailDomain
    {
        $this->logger->debug(sprintf(
            'Getting domain by domain name "%s"%s...',
            $domainName,
            $allowAliases ? ' (or domain name aliases)' : ''
        ));

        $lcDomainName = strtolower($domainName);

        $queryBuilder = $this->createQueryBuilder();
        $queryBuilder->addOr($queryBuilder->expr()->field('domain')->equals($lcDomainName));

        if ($allowAliases) {
            $queryBuilder->addOr($queryBuilder->expr()->field('aliases.domain')->equals($lcDomainName));
        }

        /** @var MailDomain $domain */
        $domain = $queryBuilder->getQuery()->getSingleResult();

        return $domain ?? throw new MailDomainNotFound($domainName);
    }
}
