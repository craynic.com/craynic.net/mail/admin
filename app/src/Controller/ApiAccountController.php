<?php

declare(strict_types=1);

namespace App\Controller;

use App\Security\MailboxUser;
use App\Service\PasswordChangeService;
use App\ValueObject\PasswordChangeStatus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/api/account', format: 'json')]
#[IsGranted('ROLE_OAUTH2_EMAIL')]
final class ApiAccountController extends AbstractController
{
    public function __construct(
        private readonly PasswordChangeService $passwordChangeService,
    ) {
    }

    #[Route('/password', name: 'password', methods: [Request::METHOD_POST])]
    public function passwordAction(Request $request, #[CurrentUser] MailboxUser $user): JsonResponse
    {
        if (!$request->request->has('currentPassword') || !$request->request->has('newPassword')) {
            throw new BadRequestHttpException();
        }

        $result = $this->passwordChangeService->changePassword(
            $user,
            trim($request->request->get('currentPassword')),
            trim($request->request->get('newPassword')),
        );

        return new JsonResponse(
            ['status' => $result->name, 'message' => $result->value],
            $result === PasswordChangeStatus::OK
                ? Response::HTTP_OK
                : Response::HTTP_FORBIDDEN
        );
    }
}
