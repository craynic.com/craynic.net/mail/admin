<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\OAuth2ConsentService;
use League\OAuth2\Server\Exception\OAuthServerException;
use Psr\Http\Message\ResponseFactoryInterface;
use Symfony\Bridge\PsrHttpMessage\HttpFoundationFactoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;
use Symfony\Component\Security\Http\Attribute\IsGranted;

final class OAuth2Controller extends AbstractController
{
    #[Route(
        '/{_locale}/oauth2/consent',
        name: 'oauth2-consent-form',
        requirements: ['_locale' => '%app.locale.route_requirements%'],
        methods: [Request::METHOD_GET, Request::METHOD_HEAD, Request::METHOD_POST],
    )]
    #[IsGranted(AuthenticatedVoter::IS_AUTHENTICATED)]
    public function consentFormAction(
        OAuth2ConsentService $consentService,
        HttpFoundationFactoryInterface $httpFoundationFactory,
        ResponseFactoryInterface $responseFactory,
        Request $request,
    ): Response {
        try {
            if ($request->getMethod() === Request::METHOD_POST) {
                $wasApproved = $request->request->has('approve')
                    && !$request->request->has('reject');

                return $consentService->processConsent($request, $wasApproved);
            }

            return $consentService->renderConsentPage($request);
        } catch (OAuthServerException $exception) {
            return $httpFoundationFactory->createResponse(
                $exception->generateHttpResponse($responseFactory->createResponse())
            );
        }
    }

    #[Route('/oauth2/authorize', name: 'oauth2-authorize', methods: [Request::METHOD_GET, Request::METHOD_HEAD])]
    public function authorizeAction(Request $request): Response
    {
        $ret = $this->forward(
            controller: 'league.oauth2_server.controller.authorization::indexAction',
            query: $request->query->all()
        );

        switch ($ret->getStatusCode()) {
            case Response::HTTP_BAD_REQUEST:
                throw new BadRequestHttpException();
            case Response::HTTP_NOT_FOUND:
                throw $this->createNotFoundException();
            case Response::HTTP_FORBIDDEN:
                throw $this->createAccessDeniedException();
            default:
                return $ret;
        }
    }
}
