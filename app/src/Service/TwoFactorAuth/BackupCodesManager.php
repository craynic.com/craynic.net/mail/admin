<?php

declare(strict_types=1);

namespace App\Service\TwoFactorAuth;

use App\Exception\TwoFactorBackupCodesNotSet;
use App\Exception\TwoFactorNotSet;
use App\Repository\MailMailboxRepository;
use App\Security\MailboxUser;
use Scheb\TwoFactorBundle\Model\BackupCodeInterface;
use Scheb\TwoFactorBundle\Model\PersisterInterface;
use Scheb\TwoFactorBundle\Security\TwoFactor\Backup\BackupCodeManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;

final readonly class BackupCodesManager implements BackupCodeManagerInterface
{
    private PasswordHasherInterface $hasher;

    public function __construct(
        private PersisterInterface $persister,
        private PasswordHasherFactoryInterface $passwordHasherFactory,
        private MailMailboxRepository $mailboxRepository,
    ) {
        $this->hasher = $this->passwordHasherFactory->getPasswordHasher(MailboxUser::class);
    }

    public function isBackupCode(object $user, string $code): bool
    {
        if ($user instanceof BackupCodeInterface) {
            return $user->isBackupCode($code);
        }

        if ($user instanceof MailboxUser) {
            try {
                $usersBackupCodes = $this
                    ->mailboxRepository
                    ->findByMailboxUser($user)
                    ->getTwoFactorAuth()
                    ->getBackupCodes();

                foreach ($usersBackupCodes as $usersBackupCode) {
                    if ($this->hasher->verify($usersBackupCode, $code)) {
                        return true;
                    }
                }
            } catch (TwoFactorNotSet | TwoFactorBackupCodesNotSet) {
            }
        }

        return false;
    }

    public function invalidateBackupCode(object $user, string $code): void
    {
        if ($user instanceof BackupCodeInterface) {
            $user->invalidateBackupCode($code);
        } elseif ($user instanceof MailboxUser) {
            try {
                $twoFactorAuth = $this->mailboxRepository->findByMailboxUser($user)->getTwoFactorAuth();
                $usersBackupCodes = $twoFactorAuth->getBackupCodes();

                foreach ($usersBackupCodes as $usersBackupCode) {
                    if ($this->hasher->verify($usersBackupCode, $code)) {
                        $twoFactorAuth->removeBackupCode($usersBackupCode);
                    }
                }
            } catch (TwoFactorNotSet | TwoFactorBackupCodesNotSet) {
            }
        }

        $this->persister->persist($user);
    }
}
