<?php

namespace App\Service;

use App\Document\MailAppPassword;
use App\Document\MailMailbox;
use Doctrine\ODM\MongoDB\DocumentManager;

final readonly class MailboxLastLoginUpdater
{
    public function __construct(private TimeProvider $timeProvider, private DocumentManager $documentManager)
    {
    }

    public function updateMailboxLastLogin(MailMailbox $mailbox): void
    {
        $mailbox->setLastLogin($this->timeProvider->getCurrentTime());

        $this->documentManager->flush();
    }

    public function updateAppPasswordLastUsed(MailAppPassword $appPassword): void
    {
        $appPassword->setLastUsed($this->timeProvider->getCurrentTime());

        $this->documentManager->flush();
    }
}
