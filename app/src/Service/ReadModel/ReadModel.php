<?php

declare(strict_types=1);

namespace App\Service\ReadModel;

use App\Document\MailDomain;

interface ReadModel
{
    /**
     * @param MailDomain[] $domains
     */
    public function refresh(array $domains): void;
}
