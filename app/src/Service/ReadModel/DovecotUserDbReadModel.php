<?php

declare(strict_types=1);

namespace App\Service\ReadModel;

use App\Entity\ReadModel\DovecotUserDb;
use App\EntityManager\ReadModelsEntityManager;
use App\Exception\QuotaNotFound;
use App\Repository\QuotaRepository;
use App\ValueObject\EmailAddress;
use App\ValueObject\ReadModelEntityCollection;
use Generator;

/**
 * @template-extends AbstractReadModel<DovecotUserDb>
 */
final class DovecotUserDbReadModel extends AbstractReadModel
{
    public function __construct(
        ReadModelsEntityManager $entityManager,
        private readonly QuotaRepository $quotaRepository,
    ) {
        parent::__construct($entityManager, DovecotUserDb::class);
    }

    protected function getAllDesiredEntities(ReadModelEntityCollection $existingRecords, array $domains): Generator
    {
        foreach ($domains as $domain) {
            if (!$domain->isActive()) {
                continue;
            }

            foreach ($domain->getMailboxes() as $mailbox) {
                if (!$mailbox->isActive()) {
                    continue;
                }

                try {
                    $quotaRuleBytes = $this->quotaRepository
                        ->findOneByDomain($domain->getName())
                        ->getQuotaAllocationForMailbox(
                            EmailAddress::fromValues($domain->getName(), $mailbox->getName())
                        );
                } catch (QuotaNotFound) {
                    $quotaRuleBytes = 0;
                }

                $newEntity = new DovecotUserDb(sprintf('%s@%s', $mailbox->getName(), $domain->getName()));
                $entity = $existingRecords->get($newEntity) ?? $newEntity;
                $entity->quotaRule = sprintf('*:bytes=%d', $quotaRuleBytes);

                yield $entity;
            }
        }
    }
}
