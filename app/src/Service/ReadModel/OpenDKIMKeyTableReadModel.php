<?php

declare(strict_types=1);

namespace App\Service\ReadModel;

use App\Entity\ReadModel\OpenDKIMKeyTable;
use App\EntityManager\ReadModelsEntityManager;
use App\Exception\DkimNotConfigured;
use App\ValueObject\ReadModelEntityCollection;
use Generator;

final class OpenDKIMKeyTableReadModel extends AbstractReadModel
{
    public function __construct(ReadModelsEntityManager $entityManager)
    {
        parent::__construct($entityManager, OpenDKIMKeyTable::class);
    }

    protected function getAllDesiredEntities(ReadModelEntityCollection $existingRecords, array $domains): Generator
    {
        foreach ($domains as $domain) {
            try {
                if (!$domain->isActive() || !$domain->getDkim()->isEnabled()) {
                    continue;
                }
            } catch (DkimNotConfigured) {
                continue;
            }

            $newEntity = new OpenDKIMKeyTable($domain->getName());
            $entity = $existingRecords->get($newEntity) ?? $newEntity;

            $domainDkim = $domain->getDkim();
            $entity->dkimKey = $domainDkim->getKey();
            $entity->dkimSelector = $domainDkim->getSelector();

            yield $entity;

            foreach ($domain->getAliases() as $domainAlias) {
                if (!$domainAlias->isActive()) {
                    continue;
                }

                $newEntity = new OpenDKIMKeyTable($domainAlias->getDomain());
                $entity = $existingRecords->get($newEntity) ?? $newEntity;
                $entity->dkimKey = $domainDkim->getKey();
                $entity->dkimSelector = $domainDkim->getSelector();

                yield $entity;
            }
        }
    }
}
