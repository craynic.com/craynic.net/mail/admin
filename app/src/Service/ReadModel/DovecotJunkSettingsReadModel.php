<?php

declare(strict_types=1);

namespace App\Service\ReadModel;

use App\Entity\ReadModel\DovecotJunkSettings;
use App\EntityManager\ReadModelsEntityManager;
use App\ValueObject\ReadModelEntityCollection;
use Generator;

/**
 * @template-extends AbstractReadModel<DovecotJunkSettings>
 */
final class DovecotJunkSettingsReadModel extends AbstractReadModel
{
    public function __construct(ReadModelsEntityManager $entityManager)
    {
        parent::__construct($entityManager, DovecotJunkSettings::class);
    }

    protected function getAllDesiredEntities(ReadModelEntityCollection $existingRecords, array $domains): Generator
    {
        foreach ($domains as $domain) {
            if (!$domain->isActive() || !$domain->isDeliveryEnabled()) {
                continue;
            }

            foreach ($domain->getMailboxes() as $mailbox) {
                if (!$mailbox->isActive() || !$mailbox->isDeliveryEnabled()) {
                    continue;
                }

                $newEntity = new DovecotJunkSettings(sprintf('%s@%s', $mailbox->getName(), $domain->getName()));
                $entity = $existingRecords->get($newEntity) ?? $newEntity;

                if (null !== $mailbox->getJunkFolderName()) {
                    $entity->junkFolderName = $mailbox->getJunkFolderName();
                    yield $entity;
                }
            }
        }
    }
}
