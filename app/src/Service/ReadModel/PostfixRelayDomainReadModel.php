<?php

declare(strict_types=1);

namespace App\Service\ReadModel;

use App\Entity\ReadModel\PostfixRelayDomain;
use App\EntityManager\ReadModelsEntityManager;
use App\ValueObject\ReadModelEntityCollection;
use Generator;

final class PostfixRelayDomainReadModel extends AbstractReadModel
{
    public function __construct(ReadModelsEntityManager $entityManager)
    {
        parent::__construct($entityManager, PostfixRelayDomain::class);
    }

    protected function getAllDesiredEntities(ReadModelEntityCollection $existingRecords, array $domains): Generator
    {
        foreach ($domains as $domain) {
            if (!$domain->isActive() || !$domain->isDeliveryEnabled()) {
                continue;
            }

            $newEntity = new PostfixRelayDomain($domain->getName());
            yield $existingRecords->get($newEntity) ?? $newEntity;

            foreach ($domain->getAliases() as $alias) {
                if (!$alias->isActive() || !$alias->isDeliveryEnabled()) {
                    continue;
                }

                $newEntity = new PostfixRelayDomain($alias->getDomain());
                yield $existingRecords->get($newEntity) ?? $newEntity;
            }
        }
    }
}
