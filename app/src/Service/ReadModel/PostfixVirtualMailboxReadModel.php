<?php

declare(strict_types=1);

namespace App\Service\ReadModel;

use App\Entity\ReadModel\PostfixVirtualMailbox;
use App\EntityManager\ReadModelsEntityManager;
use App\ValueObject\ReadModelEntityCollection;
use Generator;

final class PostfixVirtualMailboxReadModel extends AbstractReadModel
{
    public function __construct(ReadModelsEntityManager $entityManager)
    {
        parent::__construct($entityManager, PostfixVirtualMailbox::class);
    }

    protected function getAllDesiredEntities(ReadModelEntityCollection $existingRecords, array $domains): Generator
    {
        foreach ($domains as $domain) {
            if (!$domain->isActive() || !$domain->isDeliveryEnabled()) {
                continue;
            }

            foreach ($domain->getMailboxes() as $mailbox) {
                if (!$mailbox->isActive() || !$mailbox->isDeliveryEnabled()) {
                    continue;
                }

                $newEntity = new PostfixVirtualMailbox(sprintf('%s@%s', $mailbox->getName(), $domain->getName()));
                yield $existingRecords->get($newEntity) ?? $newEntity;
            }
        }
    }
}
