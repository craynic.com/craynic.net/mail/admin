<?php

declare(strict_types=1);

namespace App\Service\ReadModel;

use App\Document\MailDomain;
use App\Document\MailTargetAggregate;
use App\Entity\ReadModel\PostfixVirtualAlias;
use App\EntityManager\ReadModelsEntityManager;
use App\ValueObject\ReadModelEntityCollection;
use Generator;

final class PostfixVirtualAliasReadModel extends AbstractReadModel
{
    public function __construct(ReadModelsEntityManager $entityManager)
    {
        parent::__construct($entityManager, PostfixVirtualAlias::class);
    }

    protected function getAllDesiredEntities(ReadModelEntityCollection $existingRecords, array $domains): Generator
    {
        foreach ($domains as $domain) {
            if (!$domain->isActive() || !$domain->isDeliveryEnabled()) {
                continue;
            }

            $mainDomainName = $domain->getName();

            foreach ($domain->getMailboxes() as $mailbox) {
                if (!$mailbox->isActive() || !$mailbox->isDeliveryEnabled()) {
                    continue;
                }

                $mailboxEmail = sprintf('%s@%s', $mailbox->getName(), $mainDomainName);
                $isPureRedirect = $mailbox->getRedirect()?->isActive() && !$mailbox->getRedirect()->isKeepCopy();

                foreach ($this->getActiveDomainAliases($domain) as $domainName) {
                    // either not a redirect at all, or redirect with keepCopy
                    if (!$isPureRedirect) {
                        // do not create mailbox@domain.tld -> mailbox@domain.tld (same domain) redirect,
                        // unless this mailbox redirects e-mails and still wants to keep a copy
                        if ($domainName !== $mainDomainName || $mailbox->getRedirect()?->isActive()) {
                            $newEntity = new PostfixVirtualAlias(
                                sprintf('%s@%s', $mailbox->getName(), $domainName),
                                $mailboxEmail
                            );
                            yield $existingRecords->get($newEntity) ?? $newEntity;
                        }
                    }

                    // if this mailbox is a redirect (and domain logic implies it has active redirect targets)...
                    if ($mailbox->getRedirect()?->isActive()) {
                        foreach (
                            $this->getActiveTargetEmailAddresses($domain, $mailbox->getRedirect()) as $targetEmail
                        ) {
                            $newEntity = new PostfixVirtualAlias(
                                sprintf('%s@%s', $mailbox->getName(), $domainName),
                                $targetEmail
                            );
                            yield $existingRecords->get($newEntity) ?? $newEntity;
                        }
                    }
                }
            }

            foreach ($domain->getRedirects() as $redirect) {
                foreach ($this->getActiveTargetEmailAddresses($domain, $redirect) as $targetEmail) {
                    foreach ($this->getActiveDomainAliases($domain) as $domainName) {
                        $newEntity = new PostfixVirtualAlias(
                            sprintf('%s@%s', $redirect->getMailbox(), $domainName),
                            $targetEmail
                        );
                        yield $existingRecords->get($newEntity) ?? $newEntity;
                    }
                }
            }
        }
    }

    /**
     * @param MailDomain $domain
     * @param MailTargetAggregate $mailTargetAggregate
     * @return Generator<int, string>
     */
    private function getActiveTargetEmailAddresses(
        MailDomain $domain,
        MailTargetAggregate $mailTargetAggregate
    ): Generator
    {
        if (!$mailTargetAggregate->isActive()) {
            return;
        }

        $mainDomainName = $domain->getName();

        foreach ($mailTargetAggregate->getTargets() as $target) {
            if ($target->isActive()) {
                yield str_contains($target->getTargetAddress(), '@')
                    ? $target->getTargetAddress()
                    : sprintf('%s@%s', $target->getTargetAddress(), $mainDomainName);
            }
        }
    }

    private function getActiveDomainAliases(MailDomain $domain): Generator
    {
        if (!$domain->isActive() || !$domain->isDeliveryEnabled()) {
            return;
        }

        yield $domain->getName();

        foreach ($domain->getAliases() as $alias) {
            if (!$alias->isActive() || !$alias->isDeliveryEnabled()) {
                continue;
            }

            yield $alias->getDomain();
        }
    }
}
