<?php

declare(strict_types=1);

namespace App\Service\ReadModel;

use App\Entity\ReadModel\PostfixSenderBcc;
use App\EntityManager\ReadModelsEntityManager;
use App\ValueObject\ReadModelEntityCollection;
use Generator;

final class PostfixSenderBccReadModel extends AbstractReadModel
{
    public function __construct(ReadModelsEntityManager $entityManager)
    {
        parent::__construct($entityManager, PostfixSenderBcc::class);
    }

    protected function getAllDesiredEntities(ReadModelEntityCollection $existingRecords, array $domains): Generator
    {
        foreach ($domains as $domain) {
            if (!$domain->isActive()) {
                continue;
            }

            foreach ($domain->getMailboxes() as $mailbox) {
                if (!$mailbox->isActive() || !$mailbox->getBcc()?->isActive()) {
                    continue;
                }

                $newEntity = new PostfixSenderBcc(sprintf('%s@%s', $mailbox->getName(), $domain->getName()));
                $entity = $existingRecords->get($newEntity) ?? $newEntity;
                $entity->bcc = str_contains($mailbox->getBcc()->getBcc(), '@')
                    ? $mailbox->getBcc()->getBcc()
                    : sprintf('%s@%s', $mailbox->getBcc()->getBcc(), $domain->getName());

                yield $entity;
            }
        }
    }
}
