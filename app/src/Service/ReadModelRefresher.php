<?php

declare(strict_types=1);

namespace App\Service;

use App\Repository\MailDomainRepository;
use App\Repository\QuotaRepository;
use App\Service\ReadModel\ReadModel;
use Psr\Log\LoggerInterface;

final readonly class ReadModelRefresher
{
    /**
     * @param ReadModel[] $readModels
     */
    public function __construct(
        private iterable $readModels,
        private MailDomainRepository $mailDomainRepository,
        private QuotaRepository $quotaRepository,
        private LoggerInterface $logger
    ) {
    }

    public function refresh(): void
    {
        $this->mailDomainRepository->clear();
        $this->quotaRepository->clear();

        $domains = $this->mailDomainRepository->findAll();

        foreach ($this->readModels as $readModel) {
            $this->logger->debug(sprintf('Refreshing read model %s...', $readModel::class));
            $readModel->refresh($domains);
            $this->logger->debug(sprintf('Refreshing read model %s done.', $readModel::class));
        }
    }
}
