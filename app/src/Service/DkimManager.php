<?php

declare(strict_types=1);

namespace App\Service;
use App\Document\MailDkim;
use App\Exception\InvalidDkimKey;
use RuntimeException;

final class DkimManager
{
    private const int DEFAULT_BITS = 2048;
    private const string DEFAULT_SELECTOR = 'craynic';

    public function __construct(
        private readonly int $defaultBits = self::DEFAULT_BITS,
        private readonly string $defaultSelector = self::DEFAULT_SELECTOR,
    ) {
    }

    public function getDNSRecord(MailDkim $dkim): string
    {
        $key = openssl_pkey_get_private($dkim->getKey());
        if ($key === false) {
            throw new InvalidDkimKey();
        }

        $details = openssl_pkey_get_details($key);
        if ($details === false) {
            throw new InvalidDkimKey();
        }

        return sprintf(
            'v=DKIM1; k=rsa; p=%s',
            $this->publicPemKeyToDer($details['key'])
        );
    }

    private function publicPemKeyToDer(string $pemPublicKey): string
    {
        $begin = 'BEGIN PUBLIC KEY-----';
        $end = '-----END PUBLIC KEY';
        $beginPos = strpos($pemPublicKey, $begin) + strlen($begin);
        $endPos = strpos($pemPublicKey, $end);

        return str_replace("\n", '', substr(
            $pemPublicKey,
            $beginPos + 1,
            $endPos - strlen($pemPublicKey) - 1
        ));
    }

    public function generateNew(int $bits = null, string $selector = null): MailDkim
    {
        $key = '';

        $openSslKey = openssl_pkey_new([
            'private_key_bits' => $bits ?? $this->defaultBits,
            'private_key_type' => OPENSSL_KEYTYPE_RSA,
        ]);

        if ($openSslKey === false || !openssl_pkey_export($openSslKey, $key)) {
            throw new RuntimeException('Could not generate new private key.');
        }

        return new MailDkim(trim($key), $selector ?? $this->defaultSelector);
    }
}
