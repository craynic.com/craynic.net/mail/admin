<?php

/** @noinspection PhpUnused */

declare(strict_types=1);

namespace App\Service;

use App\Document\MailAdmin;
use App\Exception\MailboxNotFound;
use App\Exception\MailDomainNotFound;
use App\Repository\MailAdminRepository;
use App\Repository\MailDomainRepository;
use App\ValueObject\EmailAddress;
use Doctrine\ODM\MongoDB\Aggregation\Builder;
use Generator;
use MongoDB\BSON\Regex;

final readonly class ManagedAccountService
{
    public function __construct(
        private MailDomainRepository $domainRepository,
        private MailAdminRepository $adminRepository
    ) {
    }

    public function isManagedAccount(MailAdmin $admin, EmailAddress $mailboxToCheck): bool
    {
        if (!$admin->canManageDomain($mailboxToCheck->domain)) {
            return false;
        }

        try {
            $domainDocument = $this->domainRepository->findOneByDomainName($mailboxToCheck->domain);
        } catch (MailDomainNotFound) {
            return false;
        }

        if (!$domainDocument->isActive() || !$domainDocument->isLoginEnabled()) {
            return false;
        }

        try {
            $mailboxDocument = $domainDocument->getMailboxByName($mailboxToCheck->mailbox);
        } catch (MailboxNotFound) {
            return false;
        }

        if (!$mailboxDocument->isActive() || !$mailboxDocument->isLoginEnabled()) {
            return false;
        }

        return true;
    }

    /**
     * @return Generator<string>
     */
    public function findManagedAccounts(MailAdmin $admin, ?string $textToFind, int $limit): Generator
    {
        $aggregation = $admin->isSuperAdmin()
            ? $this->getAggregationBuilderForSuperAdmin()
            : $this->getAggregationBuilderForLocalAdmin($admin);

        $aggregation
            ->match()
                ->field('isActive')
                    ->equals(true)
                ->field('isLoginEnabled')
                    ->equals(true)
                ->field('mailbox.isActive')
                    ->equals(true)
                ->field('mailbox.isLoginEnabled')
                    ->equals(true)
            ->project()
                ->excludeFields(['_id'])
                ->field('email')
                    ->concat('$mailbox.mailbox', '@', '$domain');

        if (!empty($textToFind)) {
            $aggregation
                ->match()
                ->field('email')
                ->equals(new Regex($textToFind));
        }

        $aggregation->limit(min($limit, 100));

        foreach ($aggregation->getAggregation() as $document) {
            yield $document['email'];
        }
    }

    private function getAggregationBuilderForSuperAdmin(): Builder
    {
        $aggregation = $this->domainRepository->createAggregationBuilder();

        $aggregation
            ->unwind('$mailboxes')
            ->project()
                ->excludeFields(['_id'])
                ->includeFields(['domain', 'isActive', 'isLoginEnabled'])
                ->field('mailbox')
                ->expression('$mailboxes');

        return $aggregation;
    }

    private function getAggregationBuilderForLocalAdmin(MailAdmin $admin): Builder
    {
        $aggregation = $this->adminRepository->createAggregationBuilder();

        $aggregation
            ->match()
                ->field('email')
                ->equals($admin->getEmail())
            ->lookup('domains')
                ->localField('domains')
                ->foreignField('_id')
                ->alias('domains')
            ->unwind('$domains')
            ->unwind('$domains.mailboxes')
            ->project()
                ->excludeFields(['_id'])
                ->field('domain')
                ->expression('$domains.domain')
                ->field('isActive')
                ->expression('$domains.isActive')
                ->field('isLoginEnabled')
                ->expression('$domains.isLoginEnabled')
                ->field('mailbox')
                ->expression('$domains.mailboxes');

        return $aggregation;
    }
}
