<?php

namespace App\Service;

use App\Document\MailAdmin;
use App\Document\Quota;
use App\Document\QuotaDomain;
use App\Document\QuotaUsageAbstract;
use App\Exception\UnlimitedQuotaMax;
use App\Exception\ZeroQuotaMax;
use App\Repository\MailAdminRepository;
use App\Repository\MailDomainRepository;
use Generator;
use MongoDB\BSON\ObjectIdInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

final readonly class QuotaWeeklyReporter
{
    public function __construct(
        private MailAdminRepository $mailAdminRepository,
        private MailDomainRepository $mailDomainRepository,
        private MailerInterface $mailer,
        private LoggerInterface $logger,
        private string $quotaReportSender,
    ) {
    }

    public function sendReportByEmail(Quota $quota): void
    {
        $admins = $this->getAdminsForQuota($quota);

        if (empty($admins)) {
            $this->logger->debug("Not sending quota report for quota ID {$quota->getObjectId()}, no admins.");
            return;
        }

        $fromAddress = new Address($this->quotaReportSender);
        $subject = $this->getReportSubject($quota);

        foreach ($admins as $admin) {
            $email = (new Email())
                ->from($fromAddress)
                ->subject($subject)
                ->to($admin->getEmail())
                ->text($this->generateReport($quota, $admin));

            $this->logger->debug("Sending quota report for quota ID {$quota->getObjectId()} to: {$admin->getEmail()}");
            $this->mailer->send($email);
        }
    }

    private function getReportSubject(Quota $quota): string
    {
        $subject = "Quota report";

        $allDomainNames = $quota->getAllDomainNames();
        if (!empty($allDomainNames)) {
            $subject .= sprintf(' for %s', $allDomainNames[0]);

            if (count($allDomainNames) > 1) {
                $subject .= sprintf(' (+%d)', count($allDomainNames) - 1);
            }
        }

        return $subject;
    }

    /**
     * @return MailAdmin[]
     */
    private function getAdminsForQuota(Quota $quota): array
    {
        $domainIds = array_map(
            fn (QuotaDomain $mailDomain): ObjectIdInterface =>
                $this->mailDomainRepository->findOneBy(['name' => $mailDomain->getDomain()])->getObjectId(),
            $quota->getDomains()->toArray()
        );

        return $this->mailAdminRepository->getAdminsForDomainIds(...$domainIds);
    }

    private function generateReport(Quota $quota, MailAdmin $admin): string
    {
        $out = "";
        foreach ($this->generateReportLines($quota, $admin) as $line) {
            $out .= $line . "\n";
        }

        return trim($out);
    }

    private function generateReportLines(Quota $quota, MailAdmin $admin): Generator
    {
        yield 'Quota report';
        yield '============';
        yield '';
        yield "Domains included: " . implode(', ', $quota->getAllDomainNames());
        yield "Overall usage: {$this->generateQuotaUsageReportLines($quota->getUsage())}";

        $omittedDomainNames = [];
        foreach ($quota->getDomains() as $domain) {
            if (!$admin->canManageDomain($domain->getDomain())) {
                $omittedDomainNames[] = $domain->getDomain();
            }
        }

        if (!empty($omittedDomainNames)) {
            yield '';
            yield 'The following domains are part of this quota but they are omitted from this report'
                . ' since the recipient is not their administrator:';
            yield implode(', ', $omittedDomainNames);
        }

        foreach ($quota->getDomains() as $domain) {
            if (!$admin->canManageDomain($domain->getDomain())) {
                continue;
            }

            yield '';
            yield "Domain {$domain->getDomain()}";
            yield str_repeat('-', strlen($domain->getDomain()) + 7);
            yield '';
            yield "Overall usage: {$this->generateQuotaUsageReportLines($domain->getUsage())}";

            foreach ($domain->getMailboxes() as $mailbox) {
                yield "Mailbox {$mailbox->getMailbox()}@{$domain->getDomain()}:"
                    . " {$this->generateQuotaUsageReportLines($mailbox->getUsage())}";
            }
        }
    }

    private function generateQuotaUsageReportLines(QuotaUsageAbstract $quotaUsage): string
    {
        $line = $this->formatBytes($quotaUsage->getCurrent());

        try {
            $line .= " / {$this->formatBytes($quotaUsage->getMax())}";
            $line .= " ({$quotaUsage->getPercentage()}%)";
        } catch (UnlimitedQuotaMax|ZeroQuotaMax) {
        }

        return $line;
    }

    private function formatBytes(int $bytes): string
    {
        $units = ['B', 'kB', 'MB', 'GB'];

        foreach ($units as $count => $unit) {
            if ($bytes < pow(1024, $count + 1)) {
                return sprintf(
                    '%s %s',
                    number_format($bytes / pow(1024, $count), $count === 0 ? 0 : 1, ',', '.'),
                    $unit
                );
            }
        }

        return sprintf(
            '%s %s',
            number_format($bytes / pow(1024, count($units) - 1), 1, ',', '.'),
            array_pop($units)
        );
    }
}
