<?php

namespace App\Service;

use App\Exception\CompromisedPassword;
use App\Exception\WeakPassword;
use App\Repository\MailMailboxRepository;
use App\Security\MailboxUser;
use App\ValueObject\PasswordChangeStatus;
use Doctrine\ODM\MongoDB\DocumentManager;
use Rollerworks\Component\PasswordStrength\Validator\Constraints\PasswordStrength;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotCompromisedPassword;
use Symfony\Component\Validator\Validation;

final readonly class PasswordChangeService
{
    public function __construct(
        private PasswordHasherFactoryInterface $passwordHasherFactory,
        private DocumentManager $documentManager,
        private ParameterBagInterface $parameterBag,
        private MailMailboxRepository $mailboxRepository,
    ) {
    }

    public function changePassword(
        UserInterface $user,
        string $currentPassword,
        string $newPassword
    ): PasswordChangeStatus {
        if (!$user instanceof MailboxUser) {
            return PasswordChangeStatus::ERROR_WRONG_USER_TYPE;
        }

        if ($currentPassword === $newPassword) {
            return PasswordChangeStatus::ERROR_PASSWORDS_ARE_SAME;
        }

        $pwHasher = $this->passwordHasherFactory->getPasswordHasher($user);

        if (!$pwHasher->verify($user->getPassword(), $currentPassword)) {
            return PasswordChangeStatus::ERROR_WRONG_CURRENT_PASSWORD;
        }

        try {
            $this->validatePassword($newPassword);
        } catch (WeakPassword) {
            return PasswordChangeStatus::ERROR_WEAK_NEW_PASSWORD;
        } catch (CompromisedPassword) {
            return PasswordChangeStatus::ERROR_COMPROMISED_PASSWORD;
        }

        $this->mailboxRepository->findByMailboxUser($user)->setPassword($pwHasher->hash($newPassword));
        $this->documentManager->flush();

        return PasswordChangeStatus::OK;
    }

    private function validatePassword(string $newPassword): void
    {
        if (count(
            Validation::createValidator()->validate($newPassword, [
                new PasswordStrength(
                    minStrength: $this->parameterBag->get('app.validation.password.min_strength'),
                    minLength: $this->parameterBag->get('app.validation.password.min_strength')
                ),
                new NotBlank(),
            ])
        ) > 0) {
            throw new WeakPassword();
        }

        if ($this->parameterBag->get('app.validation.password.not_compromised_password_enabled')) {
            if (count(
                Validation::createValidator()->validate($newPassword, [
                    new NotCompromisedPassword(),
                ])
            ) > 0) {
                throw new CompromisedPassword();
            }
        }
    }
}
