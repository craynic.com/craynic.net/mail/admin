<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Repository\MailMailboxRepository;
use App\Security\MailboxUser;
use App\Service\MailboxLastLoginUpdater;
use JetBrains\PhpStorm\ArrayShape;
use Scheb\TwoFactorBundle\Security\Authorization\Voter\TwoFactorInProgressVoter;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

final readonly class MailboxInteractiveLoginSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private MailboxLastLoginUpdater $mailboxLastLoginUpdater,
        private MailMailboxRepository $mailboxRepository,
        private Security $security,
    ) {
    }

    #[ArrayShape([SecurityEvents::INTERACTIVE_LOGIN => "string"])]
    public static function getSubscribedEvents(): array
    {
        return [
            SecurityEvents::INTERACTIVE_LOGIN => 'resolve',
        ];
    }

    public function resolve(InteractiveLoginEvent $event): void
    {
        $user = $event->getAuthenticationToken()->getUser();

        if ($this->security->isGranted(TwoFactorInProgressVoter::IS_AUTHENTICATED_2FA_IN_PROGRESS)) {
            return;
        }

        if (!$user instanceof MailboxUser) {
            return;
        }

        $this->mailboxLastLoginUpdater->updateMailboxLastLogin(
            $this->mailboxRepository->findByMailboxUser($user)
        );
    }
}
