<?php

declare(strict_types=1);

namespace App\Exception;

use Ramsey\Uuid\UuidInterface;
use RuntimeException;

final class AliasNotFound extends RuntimeException
{
    public function __construct(public readonly UuidInterface $domainId, public readonly string $aliasIdentifier)
    {
        parent::__construct();
    }
}
