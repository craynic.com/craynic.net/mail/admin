<?php

namespace App\Exception;

use RuntimeException;

class CompromisedPassword extends RuntimeException
{
    public function __construct()
    {
        parent::__construct('The provided password is compromised.');
    }
}
