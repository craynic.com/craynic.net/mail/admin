<?php

declare(strict_types=1);

namespace App\Exception;

use RuntimeException;

final class MailTargetAlreadyExists extends RuntimeException
{
    public function __construct(public readonly string $mailTargetAddress)
    {
        parent::__construct();
    }
}
