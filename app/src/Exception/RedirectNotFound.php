<?php

declare(strict_types=1);

namespace App\Exception;

use RuntimeException;

final class RedirectNotFound extends RuntimeException
{
    public function __construct(public readonly string $redirectIdentifier)
    {
        parent::__construct();
    }
}
