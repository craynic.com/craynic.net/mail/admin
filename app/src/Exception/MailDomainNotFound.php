<?php

declare(strict_types=1);

namespace App\Exception;

use RuntimeException;

final class MailDomainNotFound extends RuntimeException
{
    public function __construct(public readonly string $domain)
    {
        parent::__construct();
    }
}
