<?php

declare(strict_types=1);

namespace App\Migrations\MySQL\ReadModel;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220413215333 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(<<<'SQL'
CREATE TABLE postfix_originating_domains (
    domain VARCHAR(255) NOT NULL,
    PRIMARY KEY(domain)
) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB
SQL);

        $this->addSql(<<<'SQL'
CREATE TABLE postfix_relay_domains (
    domain VARCHAR(255) NOT NULL,
    PRIMARY KEY(domain)
) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB
SQL);

        $this->addSql(<<<'SQL'
CREATE TABLE postfix_relay_recipient_maps (
    recipient VARCHAR(255) NOT NULL,
    PRIMARY KEY(recipient)
) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB
SQL);

        $this->addSql(<<<'SQL'
CREATE TABLE postfix_sender_bcc_maps (
    sender VARCHAR(255) NOT NULL,
    bcc VARCHAR(255) NOT NULL,
    PRIMARY KEY(sender)
) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB
SQL);

        $this->addSql(<<<'SQL'
CREATE TABLE postfix_sender_login_maps (
    user VARCHAR(255) NOT NULL,
    sender VARCHAR(255) NOT NULL,
    PRIMARY KEY(user, sender)
) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB
SQL);

        $this->addSql(<<<'SQL'
CREATE TABLE postfix_virtual_alias_domains (
    domain VARCHAR(255) NOT NULL,
    alias VARCHAR(255) NOT NULL,
    PRIMARY KEY(domain, alias)
) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB
SQL);

        $this->addSql(<<<'SQL'
CREATE TABLE postfix_virtual_alias_maps (
    address VARCHAR(255) NOT NULL,
    alias VARCHAR(255) NOT NULL,
    PRIMARY KEY(address, alias)
) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB
SQL);

        $this->addSql(<<<'SQL'
CREATE TABLE postfix_virtual_mailbox_domains (
    domain VARCHAR(255) NOT NULL,
    PRIMARY KEY(domain)
) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB
SQL);

        $this->addSql(<<<'SQL'
CREATE TABLE postfix_virtual_mailbox_maps (
    mailbox VARCHAR(255) NOT NULL,
    PRIMARY KEY(mailbox)
) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB
SQL);
    }

    public function down(Schema $schema): void
    {
        $this->addSql(/** @lang MySQL */ 'DROP TABLE postfix_originating_domains');
        $this->addSql(/** @lang MySQL */ 'DROP TABLE postfix_relay_domains');
        $this->addSql(/** @lang MySQL */ 'DROP TABLE postfix_relay_recipient_maps');
        $this->addSql(/** @lang MySQL */ 'DROP TABLE postfix_sender_bcc_maps');
        $this->addSql(/** @lang MySQL */ 'DROP TABLE postfix_sender_login_maps');
        $this->addSql(/** @lang MySQL */ 'DROP TABLE postfix_virtual_alias_domains');
        $this->addSql(/** @lang MySQL */ 'DROP TABLE postfix_virtual_alias_maps');
        $this->addSql(/** @lang MySQL */ 'DROP TABLE postfix_virtual_mailbox_domains');
        $this->addSql(/** @lang MySQL */ 'DROP TABLE postfix_virtual_mailbox_maps');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
