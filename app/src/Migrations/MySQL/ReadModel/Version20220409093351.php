<?php

declare(strict_types=1);

namespace App\Migrations\MySQL\ReadModel;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220409093351 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(<<<'SQL'
CREATE TABLE dovecot_userdb (
    user VARCHAR(255) NOT NULL, 
    quota_rule MEDIUMTEXT NOT NULL, 
    PRIMARY KEY(user)
) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB
SQL);
    }

    public function down(Schema $schema): void
    {
        $this->addSql(/** @lang MySQL */ 'DROP TABLE dovecot_userdb');
    }
}
