<?php

declare(strict_types=1);

namespace App\Migrations\MySQL\ReadModel;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220802023000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(<<<'SQL'
CREATE TABLE dovecot_junk_settings (
    username VARCHAR(255) NOT NULL,
    junk_folder_name VARCHAR(100) NOT NULL,
    PRIMARY KEY(username)
) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB
SQL);
    }

    public function down(Schema $schema): void
    {
        $this->addSql(/** @lang MySQL */'DROP TABLE dovecot_junk_settings');
    }

    public function isTransactional(): bool
    {
        return false;
    }
}
