<?php

declare(strict_types=1);

namespace App\Migrations\MySQL\Main;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220320125022 extends AbstractMigration
{
    public function isTransactional(): bool
    {
        return false;
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(<<<'SQL'
CREATE TABLE oauth2_client (
    identifier VARCHAR(32) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`,
    name VARCHAR(128) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`,
    secret VARCHAR(128) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci`,
    redirectUris TEXT CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci` COMMENT '(DC2Type:oauth2_redirect_uri)',
    grants TEXT CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci` COMMENT '(DC2Type:oauth2_grant)',
    scopes TEXT CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci` COMMENT '(DC2Type:oauth2_scope)',
    active TINYINT(1) NOT NULL,
    allowPlainTextPkce TINYINT(1) DEFAULT 0 NOT NULL,
    PRIMARY KEY(identifier)
) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = '' 
SQL);

        $this->addSql(<<<'SQL'
CREATE TABLE oauth2_access_token (
    identifier CHAR(80) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`,
    client VARCHAR(32) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`,
    expiry DATETIME NOT NULL COMMENT '(DC2Type:datetime_immutable)',
    userIdentifier VARCHAR(128) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci`,
    scopes TEXT CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci` COMMENT '(DC2Type:oauth2_scope)',
    revoked TINYINT(1) NOT NULL,
    INDEX IDX_454D9673C7440455 (client),
    CONSTRAINT FK_454D9673C7440455 FOREIGN KEY (client) REFERENCES oauth2_client (identifier) ON DELETE CASCADE,
    PRIMARY KEY(identifier)
) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = ''
SQL);

        $this->addSql(<<<'SQL'
CREATE TABLE oauth2_authorization_code (
    identifier CHAR(80) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`,
    client VARCHAR(32) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`,
    expiry DATETIME NOT NULL COMMENT '(DC2Type:datetime_immutable)',
    userIdentifier VARCHAR(128) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci`,
    scopes TEXT CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci` COMMENT '(DC2Type:oauth2_scope)',
    revoked TINYINT(1) NOT NULL,
    INDEX IDX_509FEF5FC7440455 (client),
    CONSTRAINT FK_509FEF5FC7440455 FOREIGN KEY (client) REFERENCES oauth2_client (identifier) ON DELETE CASCADE,
    PRIMARY KEY(identifier)
) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = '' 
SQL);

        $this->addSql(<<<'SQL'
CREATE TABLE oauth2_refresh_token (
    identifier CHAR(80) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`,
    access_token CHAR(80) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci`,
    expiry DATETIME NOT NULL COMMENT '(DC2Type:datetime_immutable)',
    revoked TINYINT(1) NOT NULL,
    INDEX IDX_4DD90732B6A2DD68 (access_token),
    CONSTRAINT FK_4DD90732B6A2DD68 FOREIGN KEY (access_token) REFERENCES oauth2_access_token (identifier)
        ON DELETE SET NULL,
    PRIMARY KEY(identifier)
) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = '' 
SQL);
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE oauth2_refresh_token');
        $this->addSql('DROP TABLE oauth2_authorization_code');
        $this->addSql('DROP TABLE oauth2_access_token');
        $this->addSql('DROP TABLE oauth2_client');
    }
}
