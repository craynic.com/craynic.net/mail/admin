<?php

declare(strict_types=1);

namespace App\Migrations\MongoDB;

use AntiMattr\MongoDB\Migrations\AbstractMigration;
use App\Migrations\MongoDB\Traits\Utils;
use MongoDB\Database;

class Version20230126224100 extends AbstractMigration
{
    use Utils;

    private static string $quotaCollectionName = 'quota';

    public function getDescription(): string
    {
        return 'Applies new version of the quota schema (percentage warnings)';
    }

    public function up(Database $db): void
    {
        $this->applySchema($db, static::$quotaCollectionName, 'src/Resources/quota.schema.v2.json');

        $collection = $db->{static::$quotaCollectionName};
        $collection->createIndex(
            ['usage.pctWarning.lastWarning.notificationSent' => 1],
            ['name' => 'usage.pctWarning.lastWarning.notificationSent_1'],
        );
        $collection->createIndex(
            ['usage.pctWarning.lastOk.notificationSent' => 1],
            ['name' => 'usage.pctWarning.lastOk.notificationSent_1'],
        );
        $collection->createIndex(
            ['domains.usage.pctWarning.lastWarning.notificationSent' => 1],
            ['name' => 'domains.usage.pctWarning.lastWarning.notificationSent_1'],
        );
        $collection->createIndex(
            ['domains.usage.pctWarning.lastOk.notificationSent' => 1],
            ['name' => 'domains.usage.pctWarning.lastOk.notificationSent_1'],
        );
        $collection->createIndex(
            ['domains.mailboxes.usage.pctWarning.lastWarning.notificationSent' => 1],
            ['name' => 'domains.mailboxes.usage.pctWarning.lastWarning.notificationSent_1'],
        );
        $collection->createIndex(
            ['domains.mailboxes.usage.pctWarning.lastOk.notificationSent' => 1],
            ['name' => 'domains.mailboxes.usage.pctWarning.lastOk.notificationSent_1'],
        );
    }

    public function down(Database $db): void
    {
        $this->applySchema($db, static::$quotaCollectionName, 'src/Resources/quota.schema.v1.json');

        $collection = $db->{static::$quotaCollectionName};
        $collection->updateMany([], ['$unset' => [
            'usage.pctWarning' => true,
            'domains.$[].usage.pctWarning' => true,
            'domains.$[].mailboxes.$[].usage.pctWarning' => true,
        ]]);

        $collection->dropIndex('usage.pctWarning.lastWarning.notificationSent_1');
        $collection->dropIndex('usage.pctWarning.lastOk.notificationSent_1');
        $collection->dropIndex('domains.usage.pctWarning.lastWarning.notificationSent_1');
        $collection->dropIndex('domains.usage.pctWarning.lastOk.notificationSent_1');
        $collection->dropIndex('domains.mailboxes.usage.pctWarning.lastWarning.notificationSent_1');
        $collection->dropIndex('domains.mailboxes.usage.pctWarning.lastOk.notificationSent_1');
    }
}
