<?php

declare(strict_types=1);

namespace App\Migrations\MongoDB;

use AntiMattr\MongoDB\Migrations\AbstractMigration;
use App\Migrations\MongoDB\Traits\Utils;
use MongoDB\Database;

class Version20230208190500 extends AbstractMigration
{
    use Utils;

    private static string $quotaCollectionName = 'quota';

    public function getDescription(): string
    {
        return 'Applies new version of the quota schema (optimistic locking)';
    }

    public function up(Database $db): void
    {
        $this->applySchema($db, static::$quotaCollectionName, 'src/Resources/quota.schema.v3.json');

        $collection = $db->{static::$quotaCollectionName};
        $collection->updateMany([], ['$set' => ['version' => 1]]);
    }

    public function down(Database $db): void
    {
        $this->applySchema($db, static::$quotaCollectionName, 'src/Resources/quota.schema.v2.json');

        $collection = $db->{static::$quotaCollectionName};
        $collection->updateMany([], ['$unset' => ['version' => true]]);
    }
}
