<?php

declare(strict_types=1);

namespace App\Migrations\MongoDB;

use AntiMattr\MongoDB\Migrations\AbstractMigration;
use App\Migrations\MongoDB\Traits\Utils;
use MongoDB\Database;

class Version20220907212433 extends AbstractMigration
{
    use Utils;

    private static string $quotaCollectionName = 'quota';

    public function getDescription(): string
    {
        return 'Creates the indexed quota collection with JSON schema validation';
    }

    public function up(Database $db): void
    {
        $this->createCollection($db, static::$quotaCollectionName, 'src/Resources/quota.schema.v1.json');

        $collection = $db->{static::$quotaCollectionName};

        $collection->createIndex(['id' => 1], ['unique' => true]);
        $collection->createIndex(['domains.domain' => 1], ['unique' => true]);
    }

    public function down(Database $db): void
    {
        $db->dropCollection(static::$quotaCollectionName);
    }
}
