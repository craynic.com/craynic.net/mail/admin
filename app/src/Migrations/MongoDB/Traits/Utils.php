<?php

namespace App\Migrations\MongoDB\Traits;

use MongoDB\Database;
use stdClass;

trait Utils
{
    protected function createCollection(Database $db, string $collectionName, string $schemaFileName): void
    {
        if (!$this->hasCollection($db, $collectionName)) {
            $db->createCollection(
                $collectionName,
                ['validator' => ['$jsonSchema' => $this->loadSchema($schemaFileName) ]]
            );
        } else {
            $this->applySchema($db, $collectionName, $schemaFileName);
        }
    }

    protected function applySchema(Database $db, string $collectionName, string $schemaFileName): void
    {
        $db->command([
            'collMod' => $collectionName,
            'validator' => ['$jsonSchema' => $this->loadSchema($schemaFileName) ]
        ]);
    }

    private function loadSchema(string $schemaFileName): stdClass
    {
        return json_decode(file_get_contents($schemaFileName));
    }

    private function hasCollection(Database $db, string $collectionNameToCheck): bool
    {
        foreach ($db->listCollectionNames() as $collectionName) {
            if ($collectionName === $collectionNameToCheck) {
                return true;
            }
        }

        return false;
    }
}
