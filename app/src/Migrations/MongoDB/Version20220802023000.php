<?php

declare(strict_types=1);

namespace App\Migrations\MongoDB;

use AntiMattr\MongoDB\Migrations\AbstractMigration;
use App\Migrations\MongoDB\Traits\Utils;
use MongoDB\Database;

class Version20220802023000 extends AbstractMigration
{
    use Utils;

    private static string $domainsCollectionName = 'domains';

    public function getDescription(): string
    {
        return 'Applies new version of the domains schema (add optional junkFolderName property to mailboxes)';
    }

    public function up(Database $db): void
    {
        $this->applySchema($db, static::$domainsCollectionName, 'src/Resources/domains.schema.v3.json');
    }

    public function down(Database $db): void
    {
        $this->applySchema($db, static::$domainsCollectionName, 'src/Resources/domains.schema.v2.json');

        $collection = $db->{static::$domainsCollectionName};
        $collection->updateMany([], [
            '$unset' => [
                'mailboxes.$[].junkFolderName' => true,
            ]
        ]);
    }
}
