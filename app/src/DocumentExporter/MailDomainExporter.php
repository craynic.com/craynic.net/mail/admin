<?php

declare(strict_types=1);

namespace App\DocumentExporter;

use App\Document\MailAlias;
use App\Document\MailDomain;
use App\Exception\DkimNotConfigured;

final readonly class MailDomainExporter
{
    public function __construct(
        private MailAliasExporter $aliasExporter,
        private MailMailboxExporter $mailboxExporter,
        private MailDkimExporter $dkimExporter,
        private MailRedirectExporter $redirectExporter,
    ) {
    }

    private function exportHeader(MailDomain $domain): array
    {
        return [
            'id' => $domain->getId()->toString(),
            'name' => $domain->getName(),
        ];
    }

    public function exportDetail(MailDomain $domain): array
    {
        try {
            $dkimArray = [
                'dkim' => $this->dkimExporter->exportDetail($domain->getDkim()),
            ];
        } catch (DkimNotConfigured) {
            $dkimArray = [];
        }

        return [
            'id' => $domain->getId()->toString(),
            'name' => $domain->getName(),
            'description' => $domain->getDescription(),
            'isActive' => $domain->isActive(),
            'isLoginEnabled' => $domain->isLoginEnabled(),
            'isDeliveryEnabled' => $domain->isDeliveryEnabled(),
            'aliases' => $domain->getAliases()->map(
                    $this->aliasExporter->exportDetail(...)
                )->toArray(),
            'mailboxes' => $domain->getMailboxes()->map(
                    $this->mailboxExporter->exportDetail(...)
                )->toArray(),
            'redirects' => $domain->getRedirects()->map(
                    $this->redirectExporter->exportDetail(...)
                )->toArray(),
        ] + $dkimArray;
    }

    public function exportList(MailDomain ...$domains): array
    {
        return array_map(
            fn (MailDomain $domain): array => [
                'id' => $domain->getId()->toString(),
                'domain' => $domain->getName(),
            ],
            $domains
        );
    }

    public function exportAliasList(MailDomain $domain, MailAlias ...$aliases): array
    {
        return [
            'domain' => $this->exportHeader($domain),
            'aliases' => $this->aliasExporter->exportList(...$aliases),
        ];
    }

    public function exportSingleAlias(MailDomain $domain, MailAlias $alias): array
    {
        return [
            'domain' => $this->exportHeader($domain),
            'alias' => $this->aliasExporter->exportDetail($alias),
        ];
    }
}
