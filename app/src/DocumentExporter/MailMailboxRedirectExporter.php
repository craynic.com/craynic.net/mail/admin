<?php

declare(strict_types=1);

namespace App\DocumentExporter;

use App\Document\MailMailboxRedirect;

final readonly class MailMailboxRedirectExporter
{
    public function __construct(
        private MailTargetExporter $targetExporter,
    ) {
    }

    public function exportDetail(MailMailboxRedirect $redirect): array
    {
        return [
            'targets' => $redirect->getTargets()->map(
                    $this->targetExporter->exportDetail(...),
                )->toArray(),
            'keepCopy' => $redirect->isKeepCopy(),
            'isActive' => $redirect->isActive(),
        ];
    }
}
