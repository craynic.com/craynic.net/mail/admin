<?php

declare(strict_types=1);

namespace App\Security;

use Symfony\Component\Security\Core\User\UserInterface;

interface OAuth2UserInterface extends UserInterface
{
    public function isActive(): bool;
}
