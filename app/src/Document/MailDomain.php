<?php

declare(strict_types=1);

namespace App\Document;

use App\Exception\AliasNotFound;
use App\Exception\DkimNotConfigured;
use App\Exception\MailboxNotFound;
use App\Exception\RedirectNotFound;
use App\Repository\MailDomainRepository;
use App\ValueObject\EmailAddress;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\ObjectIdInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

#[MongoDB\Document(collection: "domains", repositoryClass: MailDomainRepository::class)]
class MailDomain
{
    /** @noinspection PhpPropertyOnlyWrittenInspection */
    #[MongoDB\Id]
    private string $_id;

    /** @noinspection PhpPropertyOnlyWrittenInspection */
    #[MongoDB\Field(type: "string")]
    private string $id;

    /** @noinspection PhpPropertyOnlyWrittenInspection */
    #[MongoDB\Field(name: "domain", type: "string")]
    private string $name;

    /** @noinspection PhpPropertyOnlyWrittenInspection */
    #[MongoDB\Field(type: "string")]
    private ?string $description = null;

    /** @noinspection PhpPropertyOnlyWrittenInspection */
    #[MongoDB\Field(type: "bool")]
    private bool $isActive;

    /** @noinspection PhpPropertyOnlyWrittenInspection */
    #[MongoDB\Field(type: "bool")]
    private bool $isLoginEnabled;

    /** @noinspection PhpPropertyOnlyWrittenInspection */
    #[MongoDB\Field(type: "bool")]
    private bool $isDeliveryEnabled;

    #[MongoDB\EmbedOne(targetDocument: MailDkim::class)]
    private ?MailDkim $dkim = null;

    /** @var Collection<int, MailAlias> */
    #[MongoDB\EmbedMany(targetDocument: MailAlias::class, collectionClass: '')]
    private Collection $aliases;

    /** @var Collection<int, MailMailbox> */
    #[MongoDB\EmbedMany(targetDocument: MailMailbox::class, collectionClass: '')]
    private Collection $mailboxes;

    /** @var Collection<int, MailRedirect> */
    #[MongoDB\EmbedMany(targetDocument: MailRedirect::class, collectionClass: '')]
    private Collection $redirects;

    public function __construct()
    {
        $this->aliases = new ArrayCollection();
        $this->mailboxes = new ArrayCollection();
        $this->redirects = new ArrayCollection();
    }

    public function getObjectId(): ObjectIdInterface
    {
        return new ObjectId($this->_id);
    }

    public function getId(): UuidInterface
    {
        return Uuid::fromBytes($this->id);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function isLoginEnabled(): bool
    {
        return $this->isLoginEnabled;
    }

    public function isDeliveryEnabled(): bool
    {
        return $this->isDeliveryEnabled;
    }

    public function getDkim(): MailDkim
    {
        if ($this->dkim === null) {
            throw new DkimNotConfigured($this->getName());
        }

        return $this->dkim;
    }

    public function setDkim(?MailDkim $dkim): void
    {
        $this->dkim = $dkim;
    }

    public function enableDkim(): void
    {
        $this->getDkim()->enable();
    }

    public function disableDkim(): void
    {
        $this->getDkim()->disable();
    }

    public function assertDkimIsConfigured(): void
    {
        $this->getDkim();
    }

    /**
     * @return Collection<int, MailAlias>
     */
    public function getAliases(): Collection
    {
        return $this->aliases;
    }

    public function getAliasByIdentifier(string $aliasIdentifier): MailAlias
    {
        foreach ($this->getAliases() as $alias) {
            if (strcasecmp($alias->getDomain(), $aliasIdentifier) === 0) {
                return $alias;
            }
        }

        throw new AliasNotFound($this->getId(), $aliasIdentifier);
    }

    /**
     * @return Collection<int, MailMailbox>
     */
    public function getMailboxes(): Collection
    {
        return $this->mailboxes;
    }

    public function getMailboxByIdentifier(string $mailboxIdentifier): MailMailbox
    {
        return Uuid::isValid($mailboxIdentifier)
            ? $this->getMailboxByUUID(Uuid::fromString($mailboxIdentifier))
            : $this->getMailboxByAddress(new EmailAddress($mailboxIdentifier));
    }

    private function getMailboxByUUID(UuidInterface $uuid): MailMailbox
    {
        foreach ($this->getMailboxes() as $mailbox) {
            if ($mailbox->getId()->equals($uuid)) {
                return $mailbox;
            }
        }

        throw new MailboxNotFound($uuid->toString());
    }

    private function getMailboxByAddress(EmailAddress $mailboxAddress): MailMailbox
    {
        foreach ($this->getMailboxes() as $mailbox) {
            if (strcasecmp($mailbox->getName(), $mailboxAddress->mailbox) === 0) {
                return $mailbox;
            }
        }

        throw new MailboxNotFound((string) $mailboxAddress);
    }

    /**
     * @return Collection<int, MailRedirect>
     */
    public function getRedirects(): Collection
    {
        return $this->redirects;
    }

    public function getRedirectByIdentifier(string $redirectIdentifier): MailRedirect
    {
        return Uuid::isValid($redirectIdentifier)
            ? $this->getRedirectByUUID(Uuid::fromString($redirectIdentifier))
            : $this->getRedirectByAddress(new EmailAddress($redirectIdentifier));
    }

    private function getRedirectByUUID(UuidInterface $uuid): MailRedirect
    {
        foreach ($this->getRedirects() as $redirect) {
            if ($redirect->getId()->equals($uuid)) {
                return $redirect;
            }
        }

        throw new RedirectNotFound($uuid->toString());
    }

    private function getRedirectByAddress(EmailAddress $redirectAddress): MailRedirect
    {
        foreach ($this->getRedirects() as $redirect) {
            if (strcasecmp($redirect->getMailbox(), $redirectAddress->mailbox) === 0) {
                return $redirect;
            }
        }

        throw new RedirectNotFound((string) $redirectAddress);
    }

    public function activateRedirect(string $redirectIdentifier): void
    {
        $this->getRedirectByIdentifier($redirectIdentifier)->setActive(true);
    }

    public function deactivateRedirect(string $redirectIdentifier): void
    {
        $this->getRedirectByIdentifier($redirectIdentifier)->setActive(false);
    }

    public function addRedirectTargets(string $redirectIdentifier, string ...$targetAddresses): void
    {
        $this->getRedirectByIdentifier($redirectIdentifier)->addTargets(...$targetAddresses);
    }

    public function setRedirectTargets(string $redirectIdentifier, string ...$targetAddresses): void
    {
        $redirect = $this->getRedirectByIdentifier($redirectIdentifier);

        $redirect->setTargets(...$targetAddresses);
    }

    public function removeRedirectTargets(string $redirectIdentifier, string ...$targetAddresses): void
    {
        $this->getRedirectByIdentifier($redirectIdentifier)->removeTargets(...$targetAddresses);
    }

    public function activateRedirectTargets(string $redirectIdentifier, string ...$targets): void
    {
        $this->getRedirectByIdentifier($redirectIdentifier)->activateTargets(...$targets);
    }

    public function deactivateRedirectTargets(string $redirectIdentifier, string ...$targets): void
    {
        $this->getRedirectByIdentifier($redirectIdentifier)->deactivateTargets(...$targets);
    }

    public function getMailboxByName(string $mailboxName): MailMailbox
    {
        foreach ($this->getMailboxes() as $mailbox) {
            if ($mailbox->getName() === $mailboxName) {
                return $mailbox;
            }
        }

        throw new MailboxNotFound($mailboxName);
    }
}
