<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */

declare(strict_types=1);

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

#[MongoDB\EmbeddedDocument]
class MailExtraSender
{
    #[MongoDB\Field(type: "string")]
    private string $sender;

    #[MongoDB\Field(type: "bool")]
    private bool $isActive;

    public function getSender(): string
    {
        return $this->sender;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }
}
