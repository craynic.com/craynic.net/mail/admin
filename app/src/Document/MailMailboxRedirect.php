<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */

declare(strict_types=1);

namespace App\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

#[MongoDB\EmbeddedDocument]
class MailMailboxRedirect implements MailTargetAggregate
{
    /** @var Collection<int, MailTarget> */
    #[MongoDB\EmbedMany(targetDocument: MailTarget::class, collectionClass: '')]
    private Collection $targets;

    #[MongoDB\Field(type: "bool")]
    private bool $keepCopy;

    #[MongoDB\Field(type: "bool")]
    private bool $isActive;

    public function __construct()
    {
        $this->targets = new ArrayCollection();
    }

    public function getTargets(): Collection
    {
        return $this->targets;
    }

    public function isKeepCopy(): bool
    {
        return $this->keepCopy;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }
}
