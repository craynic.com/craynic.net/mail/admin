<?php

declare(strict_types=1);

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

#[MongoDB\EmbeddedDocument]
class QuotaUsageMandatoryMax extends QuotaUsageAbstract
{
    #[MongoDB\Field(type: "int")]
    private int $max;

    public function __construct(int $current, int $max)
    {
        parent::__construct($current);

        $this->max = $max;
    }

    public function getMax(): int
    {
        return $this->max;
    }
}
