<?php

declare(strict_types=1);

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

#[MongoDB\EmbeddedDocument]
class MailTarget
{
    #[MongoDB\Field(name: 'target', type: "string")]
    private string $targetAddress;

    #[MongoDB\Field(type: "bool")]
    private bool $isActive;

    public function __construct(string $target)
    {
        $this->targetAddress = $target;
        $this->isActive = false;
    }

    public function getTargetAddress(): string
    {
        return $this->targetAddress;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function activate(): void
    {
        $this->isActive = true;
    }

    public function deactivate(): void
    {
        $this->isActive = false;
    }

    public function toArray(): array
    {
        return [
            'target' => $this->getTargetAddress(),
            'isActive' => $this->isActive(),
        ];
    }
}
