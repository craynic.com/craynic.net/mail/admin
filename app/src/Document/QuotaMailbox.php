<?php

declare(strict_types=1);

namespace App\Document;

use App\Exception\UnlimitedMailboxQuota;
use App\Exception\UnlimitedQuotaMax;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

#[MongoDB\EmbeddedDocument]
class QuotaMailbox
{
    #[MongoDB\Field(type: "string")]
    private string $mailbox;

    #[MongoDB\EmbedOne(targetDocument: QuotaUsageOptionalMax::class)]
    private QuotaUsageOptionalMax $usage;

    public function __construct(string $mailbox, int $currentUsage)
    {
        $this->mailbox = strtolower($mailbox);
        $this->usage = new QuotaUsageOptionalMax($currentUsage);
    }

    public function getMailbox(): string
    {
        return $this->mailbox;
    }

    public function getUsage(): QuotaUsageOptionalMax
    {
        return $this->usage;
    }

    public function getAllocatedSpace(): int
    {
        try {
            return $this->getMaximumUsage();
        } catch (UnlimitedMailboxQuota) {
            return $this->getUsage()->getCurrent();
        }
    }

    public function getMaximumUsage(): int
    {
        try {
            return $this->getUsage()->getMax();
        } catch (UnlimitedQuotaMax) {
            throw new UnlimitedMailboxQuota();
        }
    }
}
