<?php

/** @noinspection PhpPropertyOnlyWrittenInspection */

declare(strict_types=1);

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

#[MongoDB\EmbeddedDocument]
class MailAlias
{
    #[MongoDB\Field(type: "string")]
    private string $domain;

    #[MongoDB\Field(type: "bool")]
    private bool $isActive;

    #[MongoDB\Field(type: "bool")]
    private bool $isDeliveryEnabled;

    public function getDomain(): string
    {
        return $this->domain;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function isDeliveryEnabled(): bool
    {
        return $this->isDeliveryEnabled;
    }
}
