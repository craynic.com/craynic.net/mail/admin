<?php

declare(strict_types=1);

namespace App\Document;

use App\Exception\TwoFactorBackupCodesAlreadyExist;
use App\Exception\TwoFactorBackupCodesNotSet;
use App\Exception\TwoFactorGoogleAuthAlreadyExists;
use App\Exception\TwoFactorGoogleAuthNotSet;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

#[MongoDB\EmbeddedDocument]
class MailMailboxTwoFactorAuth
{
    #[MongoDB\EmbedOne(targetDocument: MailMailboxTwoFactorAuthGoogleAuth::class)]
    private ?MailMailboxTwoFactorAuthGoogleAuth $googleAuth = null;

    /**
     * @var string[]|null
     */
    #[MongoDB\Field(type: 'collection')]
    private ?array $backupCodes = null;

    public function getGoogleAuth(): MailMailboxTwoFactorAuthGoogleAuth
    {
        if (null === $this->googleAuth) {
            throw new TwoFactorGoogleAuthNotSet();
        }

        return $this->googleAuth;
    }

    public function createGoogleAuth(string $secret): void
    {
        if (null !== $this->googleAuth) {
            throw new TwoFactorGoogleAuthAlreadyExists();
        }

        $this->googleAuth = new MailMailboxTwoFactorAuthGoogleAuth(
            isEnabled: false,
            secret: $secret
        );
    }

    public function setBackupCodes(string ...$backupCodes): void
    {
        if (null !== $this->backupCodes) {
            throw new TwoFactorBackupCodesAlreadyExist();
        }

        $this->backupCodes = $backupCodes;
    }

    public function removeBackupCodes(): void
    {
        if (null === $this->backupCodes) {
            throw new TwoFactorBackupCodesNotSet();
        }

        $this->backupCodes = null;
    }

    /**
     * @return string[]
     */
    public function getBackupCodes(): array
    {
        if (null === $this->backupCodes) {
            throw new TwoFactorBackupCodesNotSet();
        }

        return $this->backupCodes;
    }

    public function removeBackupCode(string $backupCodeToRemove): void
    {
        if (null === $this->backupCodes) {
            throw new TwoFactorBackupCodesNotSet();
        }

        $this->backupCodes = array_filter(
            $this->backupCodes,
            fn (string $usersBackupCode): bool => $backupCodeToRemove !== $usersBackupCode
        );
    }
}
