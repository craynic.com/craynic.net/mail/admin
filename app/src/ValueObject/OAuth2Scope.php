<?php /** @noinspection PhpCSValidationInspection */

declare(strict_types=1);

namespace App\ValueObject;

use Symfony\Component\Translation\TranslatableMessage;

use function Symfony\Component\Translation\t;

enum OAuth2Scope: string
{
    case EMAIL = 'email';

    public function getDescription(): TranslatableMessage
    {
        return match ($this) {
            self::EMAIL => t('Send and read all e-mails'),
        };
    }
}
