<?php /** @noinspection PhpCSValidationInspection */

declare(strict_types=1);

namespace App\ValueObject;

enum PasswordChangeStatus: string
{
    case OK = 'The password was changed successfully.';
    case ERROR_WRONG_USER_TYPE = 'The password change is not allowed for the selected user.';
    case ERROR_WRONG_CURRENT_PASSWORD = 'The current password is not correct.';
    case ERROR_WEAK_NEW_PASSWORD = 'The desired new password is weak. Please choose a stronger password.';
    case ERROR_COMPROMISED_PASSWORD = 'The desired new password is compromised. Please choose a different password.';
    case ERROR_PASSWORDS_ARE_SAME = 'The desired new password is same as the current password.';
}
