<?php

namespace App\ValueObject;

use ArrayIterator;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use IteratorAggregate;
use Traversable;

/**
 * @psalm-template T
 */
final class ReadModelEntityCollection implements IteratorAggregate
{
    /** @var string[] */
    private array $entityIdentifiers;

    /** @var Array<string, T> */
    private array $indexedEntities = [];

    public function __construct(EntityManagerInterface $entityManager, string $entityClassName)
    {
        $meta = $entityManager->getClassMetadata($entityClassName);
        $this->entityIdentifiers = $meta->getIdentifierFieldNames();
    }

    /**
     * @param T ...$entities
     * @return $this
     */
    public function add(object ...$entities): self
    {
        foreach ($entities as $entity) {
            if ($this->has($entity)) {
                throw new InvalidArgumentException(sprintf(
                    'Entity with index %s already exists.',
                    $this->getIndexForEntity($entity)
                ));
            }

            $this->indexedEntities[$this->getIndexForEntity($entity)] = $entity;
        }

        return $this;
    }

    /**
     * @psalm-param T $entity
     */
    public function has(object $entity): bool
    {
        return array_key_exists($this->getIndexForEntity($entity), $this->indexedEntities);
    }

    /**
     * @psalm-param T $newEntity
     * @psalm-return ?T
     */
    public function get(object $newEntity): ?object
    {
        return $this->indexedEntities[$this->getIndexForEntity($newEntity)] ?? null;
    }

    /**
     * @psalm-param T $entity
     */
    public function remove(object $entity): void
    {
        unset($this->indexedEntities[$this->getIndexForEntity($entity)]);
    }

    /**
     * @psalm-param T $entity
     */
    private function getIndexForEntity(object $entity): string
    {
        return implode('-', array_map(
            fn (string $fieldName): string => (string) $entity->$fieldName,
            $this->entityIdentifiers
        ));
    }

    /**
     * @return Traversable<string, T>
     */
    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->indexedEntities);
    }
}
