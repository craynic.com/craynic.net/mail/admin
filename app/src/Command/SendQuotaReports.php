<?php

namespace App\Command;

use App\Document\Quota;
use App\Repository\QuotaRepository;
use App\Service\QuotaWeeklyReporter;
use App\Service\TimeProvider;
use DateTimeInterface;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\LockException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('app:send-quota-reports', 'Sends the periodic mail quota reports to admins')]
class SendQuotaReports extends Command
{
    private const int FLUSH_CHANGES_ATTEMPTS = 3;

    public function __construct(
        private readonly QuotaRepository $quotaRepository,
        private readonly LoggerInterface $logger,
        private readonly QuotaWeeklyReporter $quotaWeeklyReporter,
        private readonly TimeProvider $timeProvider,
        private readonly DocumentManager $documentManager
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var Quota $quota */
        foreach ($this->quotaRepository->findAll() as $quota) {
            $this->logger->debug("Considering sending quota report for quota ID {$quota->getObjectId()}");

            $currentTime = $this->timeProvider->getCurrentTime();

            if ($quota->getReporting()->shouldSendReport($currentTime)) {
                $this->logger->debug("Sending quota report for quota ID {$quota->getObjectId()}");

                $this->quotaWeeklyReporter->sendReportByEmail($quota);

                $this->markReportAsSentAndFlush($quota, $currentTime);
            }
        }

        $this->documentManager->flush();

        return 0;
    }

    private function markReportAsSentAndFlush(Quota $quota, DateTimeInterface $currentTime): void
    {
        $attempts = self::FLUSH_CHANGES_ATTEMPTS;

        while (true) {
            try {
                $quota->getReporting()->markAsSent($currentTime);
                $this->documentManager->flush();

                return;
            } catch (LockException $e) {
                if ($attempts <= 1) {
                    throw $e;
                }

                // TODO: this does not work with readonly properties on the document :-(
                $this->documentManager->refresh($quota);
            }

            $attempts--;
        }
    }
}
