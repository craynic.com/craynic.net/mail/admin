<?php

declare(strict_types=1);

namespace App\Command;

use App\DocumentExporter\MailDkimExporter;
use App\Repository\MailDomainRepository;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('app:domains:dkim:enable', 'Enable DKIM on a given domain')]
final class DomainsDkimEnable extends Command
{
    public function __construct(
        private readonly MailDomainRepository $mailDomainRepository,
        private readonly DocumentManager $documentManager,
        private readonly MailDkimExporter $dkimExporter,
        string $name = null
    ) {
        parent::__construct($name);
    }

    public function configure(): void
    {
        $this->addArgument(
            'domainIdentifier',
            InputArgument::REQUIRED,
            'UUID or a name of the domain or its aliases to get'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $domainIdentifier = $input->getArgument('domainIdentifier');

        $domain = $this->mailDomainRepository->findOneByDomainIdentifier($domainIdentifier);
        $domain->enableDkim();

        $this->documentManager->flush();

        $output->writeln(yaml_emit($this->dkimExporter->exportDetail($domain->getDkim())));

        return self::SUCCESS;
    }
}
