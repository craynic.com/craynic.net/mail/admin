<?php

declare(strict_types=1);

namespace App\Command;

use App\Repository\MailDomainRepository;
use App\Service\DkimManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('app:domains:dkim:getdns', 'Get the DNS DKIM record for a given domain')]
final class DomainsDkimGetDNS extends Command
{
    public function __construct(
        private readonly MailDomainRepository $mailDomainRepository,
        private readonly DkimManager $DKIMManager,
        private readonly LoggerInterface $logger,
        string $name = null
    ) {
        parent::__construct($name);
    }

    public function configure(): void
    {
        $this->addArgument(
            'domainIdentifier',
            InputArgument::REQUIRED,
            'UUID or a name of the domain or its aliases to get'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $domainIdentifier = $input->getArgument('domainIdentifier');

        $domain = $this->mailDomainRepository->findOneByDomainIdentifier($domainIdentifier);

        if (!$domain->getDkim()?->isEnabled()) {
            $this->logger->warning(sprintf('DKIM for domain "%s" is not enabled.', $domainIdentifier));
        }

        $output->writeln($this->DKIMManager->getDNSRecord($domain->getDkim()));

        return self::SUCCESS;
    }
}
