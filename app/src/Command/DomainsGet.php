<?php

declare(strict_types=1);

namespace App\Command;

use App\DocumentExporter\MailDomainExporter;
use App\Repository\MailDomainRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('app:domains:get', 'Get a domain by its UUID or domain name')]
final class DomainsGet extends Command
{
    public function __construct(
        private readonly MailDomainRepository $mailDomainRepository,
        private readonly MailDomainExporter $mailDomainExporter,
        string $name = null
    ) {
        parent::__construct($name);
    }

    public function configure(): void
    {
        $this->addArgument(
            'domainIdentifier',
            InputArgument::REQUIRED,
            'UUID or a name of the domain or its aliases to get'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $domain = $this->mailDomainRepository->findOneByDomainIdentifier(
            $input->getArgument('domainIdentifier')
        );

        $output->writeln(yaml_emit($this->mailDomainExporter->exportDetail($domain)));

        return self::SUCCESS;
    }
}
