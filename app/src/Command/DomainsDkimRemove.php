<?php

declare(strict_types=1);

namespace App\Command;

use App\DocumentExporter\MailDomainExporter;
use App\Repository\MailDomainRepository;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

#[AsCommand('app:domains:dkim:remove', 'Remove DNS DKIM record from a given domain')]
final class DomainsDkimRemove extends Command
{
    public function __construct(
        private readonly MailDomainRepository $mailDomainRepository,
        private readonly DocumentManager $documentManager,
        private readonly MailDomainExporter $mailDomainExporter,
        string $name = null
    ) {
        parent::__construct($name);
    }

    public function configure(): void
    {
        $this->addArgument(
            'domainIdentifier',
            InputArgument::REQUIRED,
            'UUID or a name of the domain or its aliases to get'
        );

        $this->addOption(
            'yes',
            null,
            InputOption::VALUE_NONE,
            'Assume "yes" as an answer whether to overwrite existing DKIM key'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $domainIdentifier = $input->getArgument('domainIdentifier');

        $domain = $this->mailDomainRepository->findOneByDomainIdentifier($domainIdentifier);

        $domain->assertDkimIsConfigured();

        if (!$input->getOption('yes')) {
            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion(sprintf(
                '<info>%s [y/n]</info> ',
                sprintf(
                    'Are you sure you want to remove DKIM configuration from domain "%s"?',
                    $domainIdentifier
                )
            ));

            if (!$helper->ask($input, $output, $question)) {
                return Command::SUCCESS;
            }
        }

        $domain->setDkim(null);

        $this->documentManager->flush();

        $output->writeln(yaml_emit($this->mailDomainExporter->exportDetail($domain)));

        return self::SUCCESS;
    }
}
