<?php

declare(strict_types=1);

namespace App\Command;

use App\Document\MailAppPassword;
use App\Repository\MailDomainRepository;
use App\Security\MailboxUserProvider;
use Doctrine\ODM\MongoDB\DocumentManager;
use Random\Randomizer;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;

#[AsCommand(
    'app:domains:mailboxes:passwords:app:set',
    'Sets a new APP password for mailbox (removes all other passwords)'
)]
final class DomainsMailboxesPasswordsAppSet extends Command
{
    private const int DEFAULT_PASSWORD_LENGTH = 32;

    public function __construct(
        private readonly DocumentManager $documentManager,
        private readonly MailDomainRepository $mailDomainRepository,
        private readonly PasswordHasherFactoryInterface $passwordHasherFactory,
        private readonly MailboxUserProvider $mailboxUserProvider,
        string $name = null
    ) {
        parent::__construct($name);
    }

    public function configure(): void
    {
        $this->addArgument('mailboxIdentifier', InputArgument::REQUIRED, 'E-mail address or UUID of the mailbox');

        $this->addOption(
            'length',
            'l',
            InputOption::VALUE_REQUIRED,
            sprintf('Length of the newly generated password; defaults to %s', self::DEFAULT_PASSWORD_LENGTH),
        );

        $this->addOption(
            'description',
            'd',
            InputOption::VALUE_REQUIRED,
            'Description for the newly generated password; defaults to empty string',
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $mailboxIdentifier = $input->getArgument('mailboxIdentifier');
        $passwordLength = (int) ($input->getOption('length') ?? self::DEFAULT_PASSWORD_LENGTH);
        $passwordDescription = $input->getOption('description') ?? '';

        $domain = $this->mailDomainRepository->findOneByMailboxIdentifier($mailboxIdentifier);
        $mailbox = $domain->getMailboxByIdentifier($mailboxIdentifier);
        $user = $this->mailboxUserProvider->loadUserByIdentifier($mailboxIdentifier);

        $newPassword = (new Randomizer())->getBytesFromString(
            'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
            $passwordLength
        );
        $newPasswordHash = $this->passwordHasherFactory->getPasswordHasher($user)->hash($newPassword);
        $mailbox->setAppPassword(new MailAppPassword($newPasswordHash, $passwordDescription));

        $this->documentManager->flush();

        $output->writeln(
            sprintf('The new (and only) password for user %s is "%s".', $mailboxIdentifier, $newPassword)
        );

        return self::SUCCESS;
    }
}
