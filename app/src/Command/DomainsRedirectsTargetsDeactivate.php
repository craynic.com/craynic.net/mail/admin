<?php

declare(strict_types=1);

namespace App\Command;

use App\DocumentExporter\MailRedirectExporter;
use App\Repository\MailDomainRepository;
use Doctrine\ODM\MongoDB\DocumentManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('app:domains:redirects:targets:deactivate', 'Deactivate target(s) of a given redirect')]
final class DomainsRedirectsTargetsDeactivate extends Command
{
    public function __construct(
        private readonly DocumentManager $documentManager,
        private readonly MailDomainRepository $mailDomainRepository,
        private readonly MailRedirectExporter $redirectExporter,
        private readonly LoggerInterface $logger,
        string $name = null
    ) {
        parent::__construct($name);
    }

    public function configure(): void
    {
        $this->addArgument(
            'redirectIdentifier',
            InputArgument::REQUIRED,
            'E-mail address or UUID of the redirect'
        );

        $this->addArgument(
            'targetAddresses',
            InputArgument::IS_ARRAY | InputArgument::REQUIRED,
            'Target address(es) of the target(s) to be deactivated'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $redirectIdentifier = $input->getArgument('redirectIdentifier');
        $targetAddresses = $input->getArgument('targetAddresses');

        $this->logger->debug(
            sprintf('Deactivating targets %s for redirect %s...', implode(', ', $targetAddresses), $redirectIdentifier)
        );

        $domain = $this->mailDomainRepository->findOneByDomainIdentifier($redirectIdentifier);
        $domain->deactivateRedirectTargets($redirectIdentifier, ...$targetAddresses);

        $this->documentManager->flush();

        $output->writeln(yaml_emit($this->redirectExporter->exportDetail(
            $domain->getRedirectByIdentifier($redirectIdentifier)
        )));

        return self::SUCCESS;
    }
}
