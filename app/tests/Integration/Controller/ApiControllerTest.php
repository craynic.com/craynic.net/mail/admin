<?php

declare(strict_types=1);

namespace Tests\App\Integration\Controller;

use Symfony\Component\HttpFoundation\Request;
use Tests\App\Integration\WebTestCase;

final class ApiControllerTest extends WebTestCase
{
    public function testApiIsLive(): void
    {
        $this->browserClient->request(Request::METHOD_GET, $this->router->generate('api-index'));

        $this->assertResponseIsSuccessful();
        $this->assertJsonStringEqualsJsonString(
            json_encode(['status' => 'ok']),
            $this->browserClient->getResponse()->getContent()
        );
    }

    public function testApiEndPointWontWorkForWrongHTTPMethod(): void
    {
        $this->browserClient->request(Request::METHOD_POST, $this->router->generate('api-index'));

        $this->assertResponseStatusCodeSame(405);
    }
}
