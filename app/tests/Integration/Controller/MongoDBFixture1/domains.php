<?php

use MongoDB\BSON\Binary;
use MongoDB\Model\BSONDocument;
use Ramsey\Uuid\Uuid;

return [
    new BSONDocument([
        'id' => new Binary(
            Uuid::fromString('8d5ecf47-59fe-4f6c-a376-01456288dc77')->getBytes(),
            Binary::TYPE_UUID
        ),
        'domain' => 'example.com',
        'description' => '',
        'isActive' => true,
        'isLoginEnabled' => true,
        'isDeliveryEnabled' => true,
        'dkim' => [
            'isEnabled' => true,
            'key' => '123123123',
            'selector' => 'craynic',
        ],
        'aliases' => [
            [
                'domain' => 'example.net',
                'isActive' => true,
                'isDeliveryEnabled' => true,
            ],
        ],
        'mailboxes' => [
            [
                'id' => new Binary(
                    Uuid::fromString('daab7455-b6b5-412f-b62a-62f8eb3e2959')->getBytes(),
                    Binary::TYPE_UUID
                ),
                'mailbox' => 'active_user',
                'description' => '',
                'password' => password_hash('12345', PASSWORD_BCRYPT),
                'isActive' => true,
                'isLoginEnabled' => true,
                'isDeliveryEnabled' => true,
                'isCatchAll' => false,
                'extraSenders' => [
                    [
                        'sender' => 'boss@example.com',
                        'isActive' => true,
                    ],
                ],
                'bcc' => [
                    'bcc' => 'backup@example.com',
                    'isActive' => true,
                ],
                'appPasswords' => [
                    [
                        'password' => password_hash('abcde', PASSWORD_BCRYPT),
                        'description' => 'Mail client password',
                    ],
                    [
                        'password' => password_hash('fghij', PASSWORD_BCRYPT),
                        'description' => 'Another mail client password',
                    ],
                ],
            ],
            [
                'id' => new Binary(
                    Uuid::fromString('4b62b279-ba8a-44ab-9459-20b011c54293')->getBytes(),
                    Binary::TYPE_UUID
                ),
                'mailbox' => 'another_active_user',
                'description' => '',
                'password' => password_hash('12345', PASSWORD_BCRYPT),
                'isActive' => true,
                'isLoginEnabled' => true,
                'isDeliveryEnabled' => true,
                'isCatchAll' => false,
                'extraSenders' => [
                    [
                        'sender' => 'boss@example.com',
                        'isActive' => true,
                    ],
                ],
                'bcc' => [
                    'bcc' => 'backup@example.com',
                    'isActive' => true,
                ],
                'appPasswords' => [
                    [
                        'password' => password_hash('abcde', PASSWORD_BCRYPT),
                        'description' => 'Mail client password',
                    ],
                    [
                        'password' => password_hash('fghij', PASSWORD_BCRYPT),
                        'description' => 'Another mail client password',
                    ],
                ],
            ],
            [
                'id' => new Binary(
                    Uuid::fromString('a386ff4b-6d71-4a8b-b82e-8db2ddddf87d')->getBytes(),
                    Binary::TYPE_UUID
                ),
                'mailbox' => 'login_disabled_user',
                'description' => '',
                'password' => password_hash('12345', PASSWORD_BCRYPT),
                'isActive' => true,
                'isLoginEnabled' => false,
                'isDeliveryEnabled' => true,
                'isCatchAll' => false,
                'appPasswords' => [
                    [
                        'password' => password_hash('abcde', PASSWORD_BCRYPT),
                        'description' => 'Mail client password',
                    ],
                    [
                        'password' => password_hash('fghij', PASSWORD_BCRYPT),
                        'description' => 'Another mail client password',
                    ],
                ],
            ],
            [
                'id' => new Binary(
                    Uuid::fromString('87a20f77-34bc-4f76-a6a9-486c010ab8df')->getBytes(),
                    Binary::TYPE_UUID
                ),
                'mailbox' => 'inactive_user',
                'description' => '',
                'password' => password_hash('12345', PASSWORD_BCRYPT),
                'isActive' => false,
                'isLoginEnabled' => true,
                'isDeliveryEnabled' => true,
                'isCatchAll' => false,
                'appPasswords' => [
                    [
                        'password' => password_hash('abcde', PASSWORD_BCRYPT),
                        'description' => 'Mail client password',
                    ],
                    [
                        'password' => password_hash('fghij', PASSWORD_BCRYPT),
                        'description' => 'Another mail client password',
                    ],
                ],
            ],
        ],
        'redirects' => [
            [
                'id' => new Binary(
                    Uuid::fromString('95b64c19-d06e-4a4e-84cf-4418bae9b40f')->getBytes(),
                    Binary::TYPE_UUID
                ),
                'mailbox' => 'info',
                'description' => '',
                'targets' => [
                    [
                        'target' => 'adam@example.com',
                        'isActive' => true,
                    ], [
                        'target' => 'betty@example.com',
                        'isActive' => true,
                    ],
                ],
                'isActive' => true,
            ],
        ],
    ]),
];
