<?php

namespace Tests\App\Integration\Controller;

use App\Security\MailboxUserProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Tests\App\Integration\WebTestCase;

final class AccountControllerTest extends WebTestCase
{
    private string $defaultLocale;

    public function setUp(): void
    {
        parent::setUp();

        $this->defaultLocale = self::getContainer()->getParameter('app.locale.default');
    }

    public function testAccountPageRedirectsToLoginForAnonymousUsers(): void
    {
        $this->browserClient->request(
            Request::METHOD_GET,
            $this->router->generate('account', ['_locale' => $this->defaultLocale])
        );

        $this->assertResponseRedirects(
            $this->router->generate(
                'account-login-form',
                ['_locale' => $this->defaultLocale],
                UrlGeneratorInterface::ABSOLUTE_URL
            )
        );
    }

    public function testAccountPageShowsForLoggedUser(): void
    {
        $this->prepareMySQL();
        $this->prepareMongoDB(__DIR__ . '/MongoDBFixture1');

        /** @var MailboxUserProvider $userProvider */
        $userProvider = self::getContainer()->get(MailboxUserProvider::class);
        $user = $userProvider->loadUserByIdentifier('active_user@example.com');

        $this->browserClient->loginUser($user);

        $this->browserClient->request(
            Request::METHOD_GET,
            $this->router->generate('account', ['_locale' => $this->defaultLocale])
        );

        $this->assertResponseStatusCodeSame(200);
    }
}
