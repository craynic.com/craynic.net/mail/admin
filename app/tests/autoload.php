<?php

declare(strict_types=1);

use Symfony\Component\Dotenv\Dotenv;

require_once dirname(__DIR__) . '/vendor/autoload_runtime.php';

if (!class_exists(Dotenv::class)) {
    throw new RuntimeException('Package "symfony/dotenv" is not installed.');
}

(new Dotenv())->loadEnv(__DIR__ . '/../.env', overrideExistingVars: true);
