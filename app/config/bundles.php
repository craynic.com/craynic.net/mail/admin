<?php

/** @noinspection PhpUndefinedNamespaceInspection */
/** @noinspection PhpUndefinedClassInspection */

declare(strict_types=1);

use AntiMattr\Bundle\MongoDBMigrationsBundle\MongoDBMigrationsBundle;
use DAMA\DoctrineTestBundle\DAMADoctrineTestBundle;
use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle;
use Doctrine\Bundle\MongoDBBundle\DoctrineMongoDBBundle;
use League\Bundle\OAuth2ServerBundle\LeagueOAuth2ServerBundle;
use Rollerworks\Bundle\PasswordStrengthBundle\RollerworksPasswordStrengthBundle;
use Scheb\TwoFactorBundle\SchebTwoFactorBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\MonologBundle\MonologBundle;
use Symfony\Bundle\SecurityBundle\SecurityBundle;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\WebpackEncoreBundle\WebpackEncoreBundle;
use Twig\Extra\TwigExtraBundle\TwigExtraBundle;

return [
    DoctrineBundle::class => ['all' => true],
    DoctrineMigrationsBundle::class => ['all' => true],
    DoctrineMongoDBBundle::class => ['all' => true],
    FrameworkBundle::class => ['all' => true],
    LeagueOAuth2ServerBundle::class => ['all' => true],
    MongoDBMigrationsBundle::class => ['all' => true],
    MonologBundle::class => ['all' => true],
    SecurityBundle::class => ['all' => true],
    TwigBundle::class => ['all' => true],
    WebpackEncoreBundle::class => ['all' => true],
    DAMADoctrineTestBundle::class => ['test' => true],
    RollerworksPasswordStrengthBundle::class => ['all' => true],
    TwigExtraBundle::class => ['all' => true],
    SchebTwoFactorBundle::class => ['all' => true],
];
