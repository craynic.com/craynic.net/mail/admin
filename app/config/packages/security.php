<?php

declare(strict_types=1);

use App\Security\MailboxAppUser;
use App\Security\MailboxUser;
use App\Security\MailboxUserProvider;
use App\Security\Voter\MailboxAdminWithTwoFactorAuthVoter;
use Symfony\Component\DependencyInjection\ContainerBuilder;

return static function (ContainerBuilder $container) {
    $container->loadFromExtension(
        'security',
        [
            'firewalls' => [
                'api' => [
                    'pattern' => '^/api',
                    'stateless' => true,
                    'oauth2' => true,
                ],
                'main' => [
                    'form_login' => [
                        'login_path' => 'account-login-form',
                        'check_path' => 'account-login-form',
                        'default_target_path' => 'account',
                    ],
                    'logout' => [
                        'path' => 'account-logout',
                        'target' => 'account-login-form',
                    ],
                    'two_factor' => [
                        'auth_form_path' => '2fa_login',
                        'check_path' => '2fa_login_check',
                    ],
                    'login_throttling' => [
                        'max_attempts' => 5,
                        'interval' => '1 minute',
                    ],
                    'remember_me' => [
                        'secret' => '%kernel.secret%',
                        'lifetime' => 604800,
                    ],
                    'switch_user' => [
                        'role' => MailboxAdminWithTwoFactorAuthVoter::CAN_SWITCH_TO_ANOTHER_MAILBOX,
                        'parameter' => '_mailbox_to_access'
                    ],
                ],
            ],
            'providers' => [
                'mailbox_user_provider' => [
                    'id' => MailboxUserProvider::class,
                ],
            ],
            'password_hashers' => [
                MailboxUser::class => [
                    'algorithm' => 'bcrypt',
                    'cost' => 10,
                ],
                MailboxAppUser::class => [
                    'algorithm' => 'bcrypt',
                    'cost' => 10,
                ],
            ],
        ],
    );
};
