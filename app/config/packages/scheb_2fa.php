<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Authenticator\Token\PostAuthenticationToken;

return static function (ContainerBuilder $container) {
    $container->loadFromExtension(
        'scheb_two_factor',
        [
            'security_tokens' => [
                UsernamePasswordToken::class,
                PostAuthenticationToken::class,
            ],
            'google' => [
                'enabled' => true,
                'server_name' => '%app.2fa.google.server_name%',
                'issuer' => '%app.2fa.google.issuer%',
                'template' => 'account/2fa-form.html.twig',
                'leeway' => 5,
            ],
            'trusted_device' => [
                'enabled' => true,
                'cookie_secure' => true,
                'key' => '%app.2fa.trusted_device.encryption_key%',
            ],
            'backup_codes' => [
                'enabled' => true,
                'manager' => 'app.2fa.backup_codes_manager',
            ],
            'persister' => 'app.2fa.persister',
        ],
    );
};
